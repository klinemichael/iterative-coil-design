function finalLoops = FollowGeometry(loops, centerPoint, innerRadius, outerRadius, height, endcaps, minDist)

%======================================================%
%
%   FollowGeometry makes sure each point lies exactly on the edge of the coil
%
%%% METHOD %%%
%
%   The nearest surface to each point is found, accounting for the style of
%   the cylindrical coil (single-wound, double-wound, endcaps), and the
%   point is replaced by the nearest point on the surface of the coil.
%
%%% ARGUMENTS %%%
%
%         loops: Cell array where each cell is a matrix containing the
%                points along each loop
%
%   centerPoint: 1x3 array containing the center point of the coil
%
%   innerRadius: Inner radius of the coil
%
%   outerRadius: Outer radius of the coil, set to 0 for a single-wound coil
%
%        height: Height of the coil
%
%       endcaps: 1 if the coil has endcaps, 0 for no endcaps
%
%       minDist: Height above the coil each curve that has to wrap around
%                it is, set to 0 to keep it at height of coil, ignore if unneccesary
%
%%% OUTPUTS %%%
%
%    finalLoops: Cell array where each cell is a matrix containing the
%                points along each loop
%
%%% Created by: Michael Kline %%%
%======================================================%

if nargin < 7 || isempty(minDist)
    minDist = -1;
end

if isempty(loops)
    finalLoops = loops;
    fprintf(2, 'ERROR: No points found in FollowGeometry\n');
    return
end

% Check if it is one point
onepoint = false;
if ~iscell(loops) && size(loops, 2) == 1
    onepoint = true;
    point = loops;
    loops = cell(1);
    loops{1} = point;
end

struct = false;
if isstruct(loops{1})
    struct = true;
    potentials = zeros(length(loops));
    for i = 1:length(loops)
        potentials(i) = loops{i}.potential;
        loops{i} = loops{i}.points;
    end
end

if outerRadius > innerRadius && ~endcaps % Double Wound Without Endcaps
    %% If point isn't on geometry, force it onto geometry
    center = centerPoint(1:2);
    for i = 1:length(loops)
        for j = 1:size(loops{i}, 2)
            point = loops{i}(1:2, j);
            x = point(1) - center(1);
            y = point(2) - center(2);
            z = loops{i}(3, j) - centerPoint(3);
            r = norm(point);
            angle = atan(y/x);
            if r < innerRadius % Snap to Inner Cylinder
                if x<0
                    angle = angle + pi;
                end
                point = innerRadius * [cos(angle) sin(angle)] + center;
                if z > height/2
                    loops{i}(:, j) = [point, height/2 + centerPoint(3)]';
                elseif z < -height/2
                    loops{i}(:, j) = [point, -height/2 + centerPoint(3)]';
                else
                    loops{i}(:, j) = [point, loops{i}(3,j)]';
                end
            elseif r > outerRadius % Snap to Outer Cylinder
                if x<0
                    angle = angle + pi;
                end
                point = outerRadius * [cos(angle) sin(angle)] + center;
                if z > height/2
                    loops{i}(:, j) = [point, height/2 + centerPoint(3)]';
                elseif z < -height/2
                    loops{i}(:, j) = [point, -height/2 + centerPoint(3)]';
                else
                    loops{i}(:, j) = [point, loops{i}(3,j)]';
                end
            else % Find Nearest Surface and Snap to it
                if (outerRadius - r) > (r - innerRadius) && abs(height/2 - z) > (r - innerRadius) && abs(height/2 + z) > (r - innerRadius) % Closest to Inner Surface
                    if x<0
                        angle = angle + pi;
                    end
                    point = innerRadius * [cos(angle) sin(angle)] + center;
                    if z > height/2
                        loops{i}(:, j) = [point, height/2 + centerPoint(3)]';
                    elseif z < -height/2
                        loops{i}(:, j) = [point, -height/2 + centerPoint(3)]';
                    else
                        loops{i}(:, j) = [point, loops{i}(3,j)]';
                    end
                elseif abs(height/2 - z) > (outerRadius - r) && abs(height/2 + z) > (outerRadius - r) % Closest to Outer Surface
                    if x<0
                        angle = angle + pi;
                    end
                    point = outerRadius * [cos(angle) sin(angle)] + center;
                    if z > height/2
                        loops{i}(:, j) = [point, height/2 + centerPoint(3)]';
                    elseif z < -height/2
                        loops{i}(:, j) = [point, -height/2 + centerPoint(3)]';
                    else
                        loops{i}(:, j) = [point, loops{i}(3,j)]';
                    end
                elseif z > 0 % Closest to Top Surface
                    loops{i}(:, j) = [point', height/2 + centerPoint(3)]';
                else % Closest to Bottom Surface
                    loops{i}(:, j) = [point', -height/2 + centerPoint(3)]';
                end
            end
        end
    end
elseif outerRadius > innerRadius && endcaps % Double Wound With Endcaps
    %% If point isn't on geometry, force it onto geometry
    center = centerPoint(1:2);
    for i = 1:length(loops)
        for j = 1:size(loops{i}, 2)
            point = loops{i}(1:2, j);
            x = point(1) - center(1);
            y = point(2) - center(2);
            z = loops{i}(3, j) - centerPoint(3);
            r = norm(point);
            angle = atan(y/x);
            if x<0
                angle = angle + pi;
            end
            if r > outerRadius % Snap to Outer Cylinder
                point = outerRadius * [cos(angle) sin(angle)] + center;
                if z > height/2
                    loops{i}(:, j) = [point, height/2 + centerPoint(3)]';
                elseif z < -height/2
                    loops{i}(:, j) = [point, -height/2 + centerPoint(3)]';
                else
                    loops{i}(:, j) = [point, loops{i}(3,j)]';
                end
            else % Find Nearest Surface and Snap to it
                if abs(outerRadius - r) > abs(r - innerRadius) && abs(height/2 - z) > abs(r - innerRadius) && abs(height/2 + z) > abs(r - innerRadius) % Closest to Inner Surface
                    point = innerRadius * [cos(angle) sin(angle)] + center;
                    if z > height/2
                        loops{i}(:, j) = [point, height/2 + centerPoint(3)]';
                    elseif z < -height/2
                        loops{i}(:, j) = [point, -height/2 + centerPoint(3)]';
                    else
                        loops{i}(:, j) = [point, loops{i}(3,j)]';
                    end
                elseif abs(height/2 - z) > abs(outerRadius - r) && abs(height/2 + z) > abs(outerRadius - r) % Closest to Outer Surface
                    point = outerRadius * [cos(angle) sin(angle)] + center;
                    if z > height/2
                        loops{i}(:, j) = [point, height/2 + centerPoint(3)]';
                    elseif z < -height/2
                        loops{i}(:, j) = [point, -height/2 + centerPoint(3)]';
                    else
                        loops{i}(:, j) = [point, loops{i}(3,j)]';
                    end
                elseif z > 0 % Closest to Top Surface
                    loops{i}(:, j) = [point', height/2 + centerPoint(3)]';
                else % Closest to Bottom Surface
                    loops{i}(:, j) = [point', -height/2 + centerPoint(3)]';
                end
            end
        end
    end
else % Single Wound
    radius = innerRadius;
    %% If point isn't on geometry, force it onto geometry
    center = centerPoint(1:2);
    for i = 1:length(loops)
        for j = 1:size(loops{i}, 2)
            point = loops{i}(1:2, j);
            x = point(1) - center(1);
            y = point(2) - center(2);
            z = loops{i}(3, j) - centerPoint(3);
            angle = atan(y/x);
            if x<0
                angle = angle + pi;
            end
            point = radius * [cos(angle) sin(angle)] + center;
            if z > height/2
                loops{i}(:, j) = [point, height/2 + centerPoint(3)]';
            elseif z < -height/2
                loops{i}(:, j) = [point, -height/2 + centerPoint(3)]';
            else
                loops{i}(:, j) = [point, loops{i}(3,j)]';
            end
        end
    end

    %% If two points force wire to cross geometry, add points around geometry
    if ~endcaps && outerRadius <= innerRadius && minDist >= 0
        heightBottom = 0;
        heightTop = 0;
        for i = 1:length(loops)
            angles = zeros(1, size(loops{i}, 2));
           for j = 1:length(angles) - 1
               angles(j) = abs(acos((2 * radius^2 - norm(loops{i}(1:2, j) - loops{i}(1:2, j + 1))^2) / (2 * radius^2)));
           end
           angles(end) = abs(acos((2 * radius^2 - norm(loops{i}(1:2, end) - loops{i}(1:2, 1))^2) / (2 * radius^2)));

           for j = length(angles):-1:1
               if j == size(loops{i}, 2)
                   p2 = 1;
               else
                   p2 = j + 1;
               end
        %   if angle > something && (z == top || z == bottom)
               maxAngle = pi / 10;
               if angles(j) > maxAngle
                   numPoints = floor(angles(j) / maxAngle) + 1;
                   angle = angles(j) / (numPoints + 1);
                   point1 = loops{i}(:, j);
                   point2 = loops{i}(:, p2);
                   x1 = point1(1) - centerPoint(1); y1 = point1(2) - centerPoint(2);
                   x2 = point2(1) - centerPoint(1); y2 = point2(2) - centerPoint(2);
                   a1 = atan(y1/x1);
                   if x1 < 0
                       a1 = a1 + pi;
                   end
                   a2 = atan(y2/x2);
                   if x2 < 0
                       a2 = a2 + pi;
                   end
                   if a1 < 0
                       a1 = a1 + 2*pi;
                   end
                   if a2 < 0
                       a2 = a2 + 2*pi;
                   end
                   newPoints = zeros(3, numPoints);

                   % find height where there are no other wires
                   if point1(3) < centerPoint(3)
                       z = -1*(height/2 + heightBottom);
                       heightBottom = heightBottom + minDist;
                   else
                       z = (height/2 + heightTop);
                       heightTop = heightTop + minDist;
                   end

                   % add points every specific angle around edge
                   for k = 1:numPoints
                       if (a1 < a2 && a2 - a1 < pi) || (a1 > a2 && a1 - a2 > pi)
                           newPoints(:, k) = [radius * (cos(a1 + k * angle)); radius * (sin(a1 + k * angle)); z] + centerPoint';
        %                    newPoints(:, k) = [radius * (cos(a1 + k * angle)); radius * (sin(a1 + k * angle)); (point1(3) + k * (point2(3) - point1(3)) / (numPoints + 1))] + centerPoint';
                       else
                           newPoints(:, k) = [radius * (cos(a1 - k * angle)); radius * (sin(a1 - k * angle)); z] + centerPoint';
        %                    newPoints(:, k) = [radius * (cos(a1 - k * angle)); radius * (sin(a1 - k * angle)); (point1(3) + k * (point2(3) - point1(3)) / (numPoints + 1))] + centerPoint';
                       end
                   end
                   if p2 == 1
                       loops{i} = [loops{i}(:, 1:j), newPoints];
                   else
                       loops{i} = [loops{i}(:, 1:j), newPoints, loops{i}(:, p2:end)];
                   end
               end
           end
        end
    end
end

%% Adjust Corners (within 2E-4 of a corner)
for i = 1:length(loops)
    R = sqrt((loops{i}(1, :)-centerPoint(1)).^2 + (loops{i}(2, :)-centerPoint(2)).^2);
    Z = loops{i}(3, :)-centerPoint(3);

    distsTopInner = sqrt((R-innerRadius).^2 + (Z-height/2).^2);
    distsBottomInner = sqrt((R-innerRadius).^2 + (Z+height/2).^2);
    
    [minDist, minIdx] = min(distsTopInner);
    if minDist < 2E-4
        originalPoint = loops{i}(:, minIdx);
        
        angle = atan(originalPoint(2) / originalPoint(1));
        if originalPoint(1) < 0
            angle = angle + pi;
        end
        
        loops{i}(:, minIdx) = [innerRadius*cos(angle); innerRadius*sin(angle); height/2];
    end

    [minDist, minIdx] = min(distsBottomInner);
    if minDist < 2E-4
        originalPoint = loops{i}(:, minIdx);

        angle = atan(originalPoint(2) / originalPoint(1));
        if originalPoint(1) < 0
            angle = angle + pi;
        end

        loops{i}(:, minIdx) = [innerRadius*cos(angle); innerRadius*sin(angle); -height/2];
    end
    
    if outerRadius > innerRadius
        distsTopOuter = sqrt((R-outerRadius).^2 + (Z-height/2).^2);
        distsBottomOuter = sqrt((R-outerRadius).^2 + (Z+height/2).^2);
        
        [minDist, minIdx] = min(distsTopOuter);
        if minDist < 2E-4
            originalPoint = loops{i}(:, minIdx);

            angle = atan(originalPoint(2) / originalPoint(1));
            if originalPoint(1) < 0
                angle = angle + pi;
            end

            loops{i}(:, minIdx) = [outerRadius*cos(angle); outerRadius*sin(angle); height/2];
        end

        [minDist, minIdx] = min(distsBottomOuter);
        if minDist < 2E-4
            originalPoint = loops{i}(:, minIdx);

            angle = atan(originalPoint(2) / originalPoint(1));
            if originalPoint(1) < 0
                angle = angle + pi;
            end

            loops{i}(:, minIdx) = [outerRadius*cos(angle); outerRadius*sin(angle); -height/2];
        end
    end
end

if struct
    for i = 1:length(loops)
        finalLoops{i}.points = loops{i};
        finalLoops{i}.potential = potentials(i);
    end
else
    finalLoops = loops;
end

if onepoint
    finalLoops = finalLoops{1};
end

end