function SaveLoops(points, file)

%======================================================%
%
%   SaveLoops saves the points in 'points' in the file with name 'file'
%
%%% METHOD %%%
%
%   The saved file format will have the first three columns as the position
%   of the point (X, Y, Z). If each cell in 'points' is a struct, the third
%   column will be the potential of the curve and the fourth column will be
%   the index of the curve. If each cell in 'points' is a 3xN array, the
%   third column will be the index of the curve
%
%%% ARGUMENTS %%%
%
%   points: Cell array where each cell is either a struct containing the
%           points along the curve and the potential, or a 3xN matrix containing
%           the points along the curve
%
%     file: Name of the file to write to
%
%%% Created by: Michael Kline %%%
%======================================================%

dlmwrite(file, []);

try
    for i = 1:length(points)
        dlmwrite(file,[points{i}.points' ones(size(points{i}.points, 2), 1)*points{i}.potential ones(size(points{i}.points, 2), 1)*i],'delimiter',',','-append');
    end
catch
    for i = 1:length(points)
        dlmwrite(file,[points{i}' ones(size(points{i}, 2), 1)*i],'delimiter',',','-append');
    end
end
end