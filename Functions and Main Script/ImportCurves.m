function [newPoints, error] = ImportCurves(curves, model, model_path, plot_group, evaluation_selName, iter, centerPoint, infElementRadius, symmetryPlane1, symmetryPlane2, symmetryPlane3)

%======================================================%
%
%   ImportCurves takes the loops and calculates the resultant field, as
%       well as the new value of the scalar potential along the boundary
%
%%% METHOD %%%
%
%   Each loop is imported as an interpolation function if it doesn't
%   contain corners or a polygon if it does contain corners. Current is
%   added to each loop and the MF (Magnetic Fields) study is ran to find the
%   resultant field, then the MFNC (Magnetic Fields, No Current) study is ran
%   to find the new magnetic scalar potential. The model is then saved and
%   the error is calculated
%
%%% ARGUMENTS %%%
%
%               curves: Cell array where each cell is a matrix containing the points along
%                       each loop
%
%                model: Object containing the COMSOL model
%
%           model_path: Folder to save each model, set to '' to not save
%                       each model
%
%           plot_group: ID of the plot group in the COMSOL model to take the
%                       scalar potential from
%
%   evaluation_selName: Name of the selection to evaluate the error
%
%                 iter: Current iteration
%
%          centerPoint: 1x3 array containing the center point of the coil
%
%     infElementRadius: Radius of the infinite element domain
%
%       symmetryPlane1: The 1st plane of symmetry of the coil, options are
%                       'xy', 'yz', 'zx', 'none', or ''
%
%       symmetryPlane2: The 2nd plane of symmetry of the coil, options are
%                       'xy', 'yz', 'zx', 'none', or ''
%
%       symmetryPlane3: The 3rd plane of symmetry of the coil, options are
%                       'xy', 'yz', 'zx', 'none', or ''
%
%%% OUTPUTS %%%
%
%            newPoints: Matrix containing the points and the potential at the
%                       point. The first column is the x-coordinate, the second column is
%                       the y-coordinate, the third column is the z-coordinate, and the
%                       fourth column is the potential
%
%                error: Error of uniformity of field inside the coil.
%                       Defined as the standard deviation of
%                       log10((field - mf.normB) / field) within specified
%                       cylinder inside the coil, where field is the ideal
%                       field strength and mf.normB is the resultant field
%                       from the windings
%
%%% Created by: Michael Kline %%%
%======================================================%

%% Imports
import com.comsol.model.*;
import com.comsol.model.util.*;

%% Check Each Loop Has Enough Points
for i = length(curves):-1:1
    if size(unique(curves{i}', 'rows')', 2) <= 2
        curves(i) = [];
    end
end

for i = length(curves):-1:1
    curves{end+1} = curves{i}(:, [end, 1]);
end

%% Import Windings
disp('Importing Windings');
selName = 'Windings';
try
    model.geom('geom1').selection.create(selName, 'CumulativeSelection');
catch
end
model.geom('geom1').create('if2', 'If');
model.geom("geom1").feature("if2").set("condition", "wires_on==1");
for j = 1:length(curves)
%     model.geom('geom1').feature.create(['pol' num2str(j)], 'Polygon').set('source', 'table').set('table', points{j}').set('type', 'closed').set('contributeto', selName);
    
    if HasCorners(curves{j})
        model.geom('geom1').feature.create(['pol' num2str(j)], 'Polygon').set('source', 'table').set('table', curves{j}').set('type', 'open').set('contributeto', selName);
    else
        model.geom('geom1').feature.create(['ic' num2str(j)], 'InterpolationCurve').set('source', 'table').set('table', curves{j}').set('rtol', 0.001).set('contributeto', selName).set('type', 'open');
    end
end

%% Find Dimensions of Intersection Block (To account for symmetry)
minX = centerPoint(1) - infElementRadius; sizeX = 2*infElementRadius;
minY = centerPoint(2) - infElementRadius; sizeY = 2*infElementRadius;
minZ = centerPoint(3) - infElementRadius; sizeZ = 2*infElementRadius;

if strcmp(symmetryPlane1, 'xy')
    minZ = centerPoint(3);
    sizeZ = infElementRadius;
elseif strcmp(symmetryPlane1, 'yz')
    minX = centerPoint(1);
    sizeX = infElementRadius;
elseif strcmp(symmetryPlane1, 'zx')
    minY = centerPoint(2);
    sizeY = infElementRadius;
end

if strcmp(symmetryPlane2, 'xy')
    minZ = centerPoint(3);
    sizeZ = infElementRadius;
elseif strcmp(symmetryPlane2, 'yz')
    minX = centerPoint(1);
    sizeX = infElementRadius;
elseif strcmp(symmetryPlane2, 'zx')
    minY = centerPoint(2);
    sizeY = infElementRadius;
end

if strcmp(symmetryPlane3, 'xy')
    minZ = centerPoint(3);
    sizeZ = infElementRadius;
elseif strcmp(symmetryPlane3, 'yz')
    minX = centerPoint(1);
    sizeX = infElementRadius;
elseif strcmp(symmetryPlane3, 'zx')
    minY = centerPoint(2);
    sizeY = infElementRadius;
end

model.geom('geom1').create('WindingsBlock', 'Block');
model.geom('geom1').feature('WindingsBlock').set('size', [sizeX; sizeY; sizeZ]);
model.geom('geom1').feature('WindingsBlock').set('pos', [minX; minY; minZ]);

model.geom('geom1').create('WindingsUnion', 'Union');
model.geom('geom1').feature('WindingsUnion').selection('input').named('Windings');

model.geom('geom1').create('WindingsIntersection', 'Intersection');
model.geom('geom1').feature('WindingsIntersection').selection('input').set({'WindingsBlock', 'WindingsUnion'});

model.geom('geom1').create('endif2', 'EndIf');

%% Set wires_on to 1 and Build Geometry
disp('Building Geometry');
model.param.set('wires_on', '1', '1 for wires, 2 for box');
model.geom('geom1').run;

%% Set Mesh Around Windings
model.mesh('mesh1').feature('edg1').selection.named(['geom1_' selName '_edg']);

%% Add Current to Wires
model.physics('mf').feature('edc1').selection.named(['geom1_' selName '_edg']); % Specify All Wires In Selection

%% Run MF Study
tic
disp('Running MF Study');
model.study('std1').run();
time = toc;
disp(['Elapsed time is ' num2str(floor(time / 60)) 'min and ' num2str(floor(rem(time, 60))) 'sec']); 

%% Run MFNC Study
tic
disp('Running MFNC Study');
model.study('std2').run();
time = toc;
disp(['Elapsed time is ' num2str(floor(time / 60)) 'min and ' num2str(floor(rem(time, 60))) 'sec']); 

%% Extract New Scalar Potential
disp('Extracting Scalar Potential Along Boundaries');
figure(10),clf
rawData = mphplot(model, plot_group);
data = {};
for i = 1:length(rawData)
    for j = 1:length(rawData{i})
        if strcmp(rawData{i}{j}.plottype, 'Surface')
            data{end+1} = rawData{i}{j}.p;
            data{end} = [data{end}' rawData{i}{j}.d];
            data{end}(any(isnan(data{end}), 2), :) = [];
        end
    end
end
newPoints = data;

%% Calculate Error
disp('Calculating Error');
expr = 'log10(sqrt((TargetBx - mf.Bx)^2 + (TargetBy - mf.By)^2 + (TargetBz - mf.Bz)^2) / sqrt(TargetBx^2 + TargetBy^2 + TargetBz^2))'; % Maybe take out log10 if it's zero even with wires. It's nonzero without log10 without wires
error = mphint2(model, expr, 'volume', 'selection', evaluation_selName);

%% Save Model
if ~strcmp(model_path, '')
    disp('Saving Model');
    try
        mphsave(model, [model_path '/Iter' num2str(iter) '.mph']);
    catch
        fprintf(2, 'ERROR: Could not save model');
    end
end

end