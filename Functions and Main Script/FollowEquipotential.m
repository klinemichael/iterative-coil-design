function [curve, potentials] = FollowEquipotential(data, us)

%======================================================%
%
%   FollowEquipotential takes the value of the scalar potential on one 2D surface
%       and finds the equipotential curves
%
%%% METHOD %%%
%
%   A fit is applied and the value of the potential is interpolated into a
%   regular grid. This is then used to find the contours using the MATLAB
%   function contour3. The final curves are then extracted. It then checks
%   to make sure the direction of the current is correct before returning
%   the values.
%
%%% ARGUMENTS %%%
%
%         data: Matrix containing the points and the potential at the
%               point. The first column is the x-coordinate, the second
%               column is the y-coordinate, the third column is the
%               z-coordinate, and the fourth column is the potential
%
%           us: Value of each potential to find the equipotential curve
%
%%% OUTPUTS %%%
%
%        curve: Cell array where each cell is a struct containing the
%               points along the equipotential and the potential of the curve
%
%   potentials: Array of the same length as curve specifying the potential
%               of each curve
%
%%% Created by: Michael Kline %%%
%======================================================%

if isscalar(us)
    us = [us, us];
end

data(:, 1) = abs(data(:, 1));
data(:, 2) = abs(data(:, 2));

minX = min(data(:, 1)); if minX < 1E-6, minX = 0; end
maxX = max(data(:, 1));
minY = min(data(:, 2)); if minY < 1E-6, minY = 0; end
maxY = max(data(:, 2));

data(find(ismembertol(data(:, 1), minX, 1E-6)), 1) = minX;
data(find(ismembertol(data(:, 1), maxX, 1E-6)), 1) = maxX;
data(find(ismembertol(data(:, 2), minY, 1E-6)), 2) = minY;
data(find(ismembertol(data(:, 2), maxY, 1E-6)), 2) = maxY;

f = fit(data(:, 1:2), data(:, 3), 'linearinterp');

[X, Y] = meshgrid(linspace(minX, maxX), linspace(minY, maxY));
Z = f(X, Y);

% figure,clf
% try
%     surf(X, Y, Z);
% catch
% end
% hold on
C = contour3(X, Y, Z, us, '-r', 'LineWidth', 2, 'Visible', 'off');
% hold off

S = contourdata(C);

curve = cell(1, length(S));
potentials = zeros(1, length(S));
for i = 1:length(S)
    curve{i} = [S(i).xdata, S(i).ydata]';
    potentials(i) = S(i).level;
end

%% Check Direction of Current
for i = 1:length(curve)
    dotprod = 0;
    for j = 2:size(curve{i}, 2)-1
        points = curve{i}(:, j:j+1);
        dirCurve = points(:, 2) - points(:, 1);
        [gradx, grady] = differentiate(f, points(1:2, 1)');
        dirCurrent = cross([0, 0, 1], [gradx, grady, 0]);
        dotprod = dotprod + dot(dirCurve, dirCurrent(1:2));
    end
    
    if dotprod < 0
        curve{i} = fliplr(curve{i});
    end
end

%% Parse Contour Matrix
function s=contourdata(c)
%CONTOURDATA Extract Contour Data from Contour Matrix C.
% CONTOUR, CONTOURF, CONTOUR3, and CONTOURC all produce a contour matrix
% C that is traditionally used by CLABEL for creating contour labels.
%
% S = CONTOURDATA(C) extracts the (x,y) data pairs describing each contour
% line and other data from the contour matrix C. The vector array structure
% S returned has the following fields:
%
% S(k).level contains the contour level height of the k-th line.
% S(k).numel contains the number of points describing the k-th line.
% S(k).isopen is True if the k-th contour is open and False if it is closed.
% S(k).xdata contains the x-axis data for the k-th line as a column vector.
% S(k).ydata contains the y-axis data for the k-th line as a column vector.
%
% For example: PLOT(S(k).xdata,S(k).ydata)) plots just the k-th contour.
%
% See also CONTOUR, CONTOURF, CONTOUR3, CONTOURC.
% From the help text of CONTOURC:
%   The contour matrix C is a two row matrix of contour lines. Each
%   contiguous drawing segment contains the value of the contour, 
%   the number of (x,y) drawing pairs, and the pairs themselves.  
%   The segments are appended end-to-end as
% 
%       C = [level1 x1 x2 x3 ... level2 x2 x2 x3 ...;
%            pairs1 y1 y2 y3 ... pairs2 y2 y2 y3 ...]
% D.C. Hanselman, University of Maine, Orono, ME 04469
% MasteringMatlab@yahoo.com
% Mastering MATLAB 7
% 2007-05-22
if nargin<1 || ~isfloat(c) || size(c,1)~=2 || size(c,2)<4
   error('CONTOURDATA:rhs', 'Input Must be the 2-by-N Contour Matrix C.')
end
tol=1e-12;
k=1;
col=1;
while col<size(c,2)
   s(k).level = c(1,col);
   s(k).numel = c(2,col);
   idx=col+1:col+c(2,col);
   s(k).xdata = c(1,idx).';
   s(k).ydata = c(2,idx).';
   s(k).isopen = abs(diff(c(1,idx([1 end]))))>tol || abs(diff(c(2,idx([1 end]))))>tol;
   k=k+1;
   col=col+c(2,col)+1;
end
end

end