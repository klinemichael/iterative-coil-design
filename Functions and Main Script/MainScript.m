import com.comsol.model.*;
import com.comsol.model.util.*;
ModelUtil.showProgress(true);

model_path = 'C:/Users/Michael/Desktop/CreateCoil/';

%============================ PSI Coil ===================================%
% model_file = [model_path, 'PSI Coil.mph'];
% plot_group = 'pg2'; % plot_group = 'pg1';
% evaluation_selName = 'cyl4';
% centerPoint = [0, 0, 0];
% innerRadius = 0.11;
% outerRadius = 0; % 0 for single-wound
% height = 1;
% endcaps = 0; % 0 for no endcaps, 1 for endcaps
% numContours = 5;
% 
% straightLineTol = 1e-5;
% smoothParam = 0.01; % 0-1, 0 is linear fit, 1 is natural cubic spline
% minDist = 5e-3; % minimum distance between adjacent points
% 
% % Symmetry Planes, up to 1/8 symmetry
% %   - Options: 'xy', 'yz', 'zx', 'none', or ''
% symmetryPlane1 = 'yz';
% symmetryPlane2 = 'zx';
% symmetryPlane3 = 'xy';
% 
% infElementRadius = 3.5; % Radius of infinite element domain
% 
% % Set to true if the potential needs to be flipped
% %   - One example is if you find the scalar potential based on the field outside the coil
% flipPotential = true; 
%=========================================================================%

%====================== Double Cos-Theta Coil ============================%
model_file = [model_path, 'DoubleCosThetaCoil.mph'];
plot_group = 'pg2';
evaluation_selName = 'cyl4';
centerPoint = [0, 0, 0];
innerRadius = 5 * 0.0254;
outerRadius = 7 * 0.0254; % 0 for single-wound
height = 8 * 0.0254;
endcaps = 1; % 0 for no endcaps, 1 for endcaps
numContours = [7, 12];

straightLineTol = 1e-5;
smoothParam = 0.002; % 0-1, 0 is linear fit, 1 is natural cubic spline
minDist = 5e-3; % minimum distance between adjacent points

% Symmetry Planes, up to 1/8 symmetry
%   - Options: 'xy', 'yz', 'zx', 'none', or ''
symmetryPlane1 = 'xy';
symmetryPlane2 = 'yz';
symmetryPlane3 = 'zx';

infElementRadius = 2.5; % Radius of infinite element domain

% Set to true if the potential needs to be flipped
%   - One example is if you find the scalar potential based on the field outside the coil
flipPotential = false; 
%=========================================================================%

%% Parameters
maxIters = 5;

%% Load COMSOL Model and Contour Plot
disp('Extracting Scalar Potential Along Boundaries');
data = ExtractScalarPotential(model_file, plot_group);

%% Find Planes with Uniform Scalar Potential
constantU = FindConstantU(data, centerPoint);

%% Find Corrections to the Windings
allLoops = {};
error = zeros(1, maxIters);
for i = 1:maxIters
    disp(['%%%%%%%%%%%%%% Iteration ' num2str(i) ' %%%%%%%%%%%%%%']);
    
    %% Flip Scalar Potential, if Necessary
    if flipPotential
        for j = 1:length(data)
            data{j}(:, 4) = -data{j}(:, 4);
        end
    end
    
    %% Find Equipotentials
    disp('Finding Equipotential Contours');
    contour = {};
    tic
    for j = 1:length(data)
        contour = [contour, FindContours(data{j}, centerPoint, innerRadius, outerRadius, height, endcaps, numContours(j), constantU)];
    end
    time = toc;
    disp(['Elapsed time is ' num2str(floor(time / 60)) 'min and ' num2str(floor(rem(time, 60))) 'sec']); 

    %% Find Straight Lines
    disp('Finding Straight Lines');
    adjustedPoints = FindStraightLines(contour, straightLineTol);

    %% Find Cubic Spline And Make New Points
    disp('Finding Cubic Spline');
    smoothedPoints = SmoothingSpline(adjustedPoints, smoothParam, minDist);

    %% Mirror Points for Symmetry
    disp('Mirroring Curves for Symmetry');
    mirroredPoints = MirrorCurves(smoothedPoints, centerPoint, symmetryPlane1, symmetryPlane2, symmetryPlane3, constantU);

    %% Re-joining Curves to Form Closed Loops
    disp('Closing Loops');
    loops = CloseLoops(mirroredPoints);
    
    %% If Multiple Points are Close Together, Replace Them with One Point
    disp('Fixing Points Too Close to Each Other');
    finalLoops = ReplaceDuplicatePoints(loops, minDist/2);
    
    %% Force Loops to Follow Geometry
    disp('Snapping Loops to Geometry');
    finalLoops = FollowGeometry(finalLoops, centerPoint, innerRadius, outerRadius, height, endcaps, minDist);
    
    %% Plot Coil
    figure(1),clf
    PlotCoil(finalLoops);
    title('Full Coil');
    
    %% Save Loops
    file_name = [model_path 'LoopsIter' num2str(i) '.csv'];
    SaveLoops(finalLoops, file_name);
    
    %% Import Windings Into Model
    model = mphopen(model_file);
    [data, error(i)] = ImportCurves(finalLoops, model, model_path, plot_group, evaluation_selName, i, centerPoint, infElementRadius, symmetryPlane1, symmetryPlane2, symmetryPlane3);
    
    %% Check for Stopping Condition
    if (i > maxIters/2 || i > 10) && i > 1
        normalizedError = error / error(1);
        disp(normalizedError(1:i));
        
        if abs(normalizedError(i) - normalizedError(i-1)) < 1e-4
            disp('Stopping Condition Met');
            return
        end
    end
end