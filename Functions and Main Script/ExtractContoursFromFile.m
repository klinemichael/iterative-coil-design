function data = ExtractContoursFromFile(data_file)

%======================================================%
%
%   ExtractContoursFromFile takes the path to a file, and reads in the
%   curves at each potential level.
%
%%% METHOD %%%
%
%   The file is read into a matrix using csvread, then the curves are split
%   into cells according to the index of the curve, located in the 4th column
%
%%% ARGUMENTS %%%
%
%   data_file: Path to the file where the contours are stored
%
%%% OUTPUTS
%
%        data: Cell array where each cell contains the points along a curve
%
%%% Created by: Michael Kline %%%
%======================================================%

rawData = csvread(data_file);
data = {};
numEquipotentials = length(unique(rawData(:,4)));
for j = 1:numEquipotentials
    equipotentialLevel = rawData(1,4);
    index = find(rawData(:,4) == equipotentialLevel);
    data{j} = rawData(1:length(index), 1:3)';
    rawData = rawData(length(index)+1:end, :);
end

end