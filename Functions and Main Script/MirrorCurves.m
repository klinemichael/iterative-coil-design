function mirroredPoints = MirrorCurves(points, centerPoint, symmetryPlane1, symmetryPlane2, symmetryPlane3, uniformU)

%======================================================%
%
%   MirrorCurves takes each curve and the symmetry and mirrors the curves
%       according to the symmetry of the coil
%
%%% METHOD %%%
%
%   For each symmetry plane, the curves are mirrored across the plane of
%   symmetry until the full coil is reconstructed
%
%%% ARGUMENTS %%%
%
%           points: Cell array where each cell is a struct containing the
%                   points along the equipotential and the potential of the curve
%
%      centerPoint: 1x3 array containing the center point of the coil
%
%   symmetryPlane1: The 1st plane of symmetry of the coil, options are 'xy',
%                   'yz', 'zx', 'none', or ''
%
%   symmetryPlane2: The 2nd plane of symmetry of the coil, options are 'xy',
%                   'yz', 'zx', 'none', or ''
%
%   symmetryPlane3: The 3rd plane of symmetry of the coil, options are 'xy',
%                   'yz', 'zx', 'none', or ''
%
%         uniformU: 1x3 array where value is 1 if plane has uniform scalar
%                   potential and 0 otherwise. Index order is 'xy', 'yz',
%                   'zx'
%
%%% OUTPUTS %%%
%
%   mirroredPoints: Cell array where each cell is a struct containing the
%                   points along the equipotential and the potential of the curve
%
%%% Created by: Michael Kline %%%
%======================================================%

if strcmp(symmetryPlane3, 'xy')
    points = FlipXY(points);
elseif strcmp(symmetryPlane3, 'yz')
    points = FlipYZ(points);
elseif strcmp(symmetryPlane3, 'zx')
    points = FlipZX(points);
end

if strcmp(symmetryPlane2, 'xy')
    points = FlipXY(points);
elseif strcmp(symmetryPlane2, 'yz')
    points = FlipYZ(points);
elseif strcmp(symmetryPlane2, 'zx')
    points = FlipZX(points);
end

if strcmp(symmetryPlane1, 'xy')
    points = FlipXY(points);
elseif strcmp(symmetryPlane1, 'yz')
    points = FlipYZ(points);
elseif strcmp(symmetryPlane1, 'zx')
    points = FlipZX(points);
end

mirroredPoints = points;

    function points = FlipXY(points)
        for i = 1:length(points)
            curve = fliplr(points{i}.points);
           for j = 1:size(curve, 2)
               curve(3, j) = centerPoint(3) - (curve(3, j) - centerPoint(3));
           end
           points{end+1}.points = curve;
           if uniformU(1)
               points{end}.potential = -points{i}.potential;
           else
               points{end}.potential = points{i}.potential;
           end
           points{end}.direction = 'Unknown';
        end
    end

    function points = FlipYZ(points)
        for i = 1:length(points)
           curve = fliplr(points{i}.points);
           for j = 1:size(curve, 2)
               curve(1, j) = centerPoint(1) - (curve(1, j) - centerPoint(1));
           end
           points{end+1}.points = curve;
           if uniformU(2)
               points{end}.potential = -points{i}.potential;
           else
               points{end}.potential = points{i}.potential;
           end
           points{end}.direction = 'Unknown';
        end
    end

    function points = FlipZX(points)
        for i = 1:length(points)
           curve = points{i}.points;
           for j = 1:size(curve, 2)
               curve(2, j) = centerPoint(2) - (curve(2, j) - centerPoint(2));
           end
           points{end+1}.points = curve;
           if uniformU(3)
               points{end}.potential = -points{i}.potential;
           else
               points{end}.potential = points{i}.potential;
           end
           points{end}.direction = 'Unknown';
       end
    end

end