function uniformU = FindConstantU(data, centerPoint)

%======================================================%
%
%   FindConstantU finds which planes have the same potential, these planes will
%   be used to change the potential when mirroring across the plane. Those
%   with non-uniform potential will keep the same potential once mirrored.
%
%%% METHOD %%%
%
%   If the standard deviation of the potentals on each face are less than
%   1E-6, then the plane has uniform scalar potential
%
%%% ARGUMENTS %%%
%
%          data: Cell array where each cell is a matrix containing the points
%                and the potential at the point. The first column is the
%                x-coordinate, the second column is the y-coordinate, the
%                third column is the z-coordinate, and the fourth column is
%                the potential
%
%   centerPoint: 1x3 array containing the center point of the coil
%
%%% OUTPUTS %%%
%
%      uniformU: 1x3 array where value is 1 if plane has uniform scalar
%                potential and 0 otherwise. Index order is 'xy', 'yz',
%                'zx'
%
%%% Created by: Michael Kline %%%
%======================================================%

points = [];
for i = 1:length(data)
    points = [points; data{i}];
end

xEqualsZero = points(find(ismembertol(points(:, 1)-centerPoint(1), 0, 1E-6)), :); stdPotentialx0 = std(xEqualsZero(:, 4));
yEqualsZero = points(find(ismembertol(points(:, 2)-centerPoint(2), 0, 1E-6)), :); stdPotentialy0 = std(yEqualsZero(:, 4));
zEqualsZero = points(find(ismembertol(points(:, 3)-centerPoint(3), 0, 1E-6)), :); stdPotentialz0 = std(zEqualsZero(:, 4));

uniformU = zeros(1, 3);

% XY-Plane
if stdPotentialz0 < 1E-6
    uniformU(1) = 1;
end

% YZ-Plane
if stdPotentialx0 < 1E-6
    uniformU(2) = 1;
end

% ZX-Plane
if stdPotentialy0 < 1E-6
    uniformU(3) = 1;
end

end