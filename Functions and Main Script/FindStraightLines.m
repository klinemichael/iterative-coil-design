function adjustedPoints = FindStraightLines(curves, tol)

%======================================================%
%
%   FindStraightLines finds if each curve is a straight line and if so, replaces the
%       curve with its two endpoints
%
%%% METHOD %%%
%
%   Each curve is fit with a linear least squares regression with perpendicular
%   offsets and if the error is within a tolerance value, the curve is a
%   straight line and is replaced by its two endpoints
%
%%% ARGUMENTS %%%
%
%             curves: Cell array where each cell is a struct containing the
%                     points along the equipotential and the potential of the curve
%
%                tol: Tolerance value used to decide if each curve is a line
%
%%% OUTPUTS %%%
%
%     adjustedPoints: Cell array where each cell is a struct containing the
%                     points along the equipotential and the potential of the curve
%
%%% NOTE %%%
%
%   The algorithm for finding the linear least-squares regression with
%   perpendicular offsets came from a paper that I can no longer find
%
%%% Created by: Michael Kline %%%
%======================================================%

adjustedPoints = curves;
for j = 1:length(curves)
    X = curves{j}.points(1, :);
    Y = curves{j}.points(2, :);
    Z = curves{j}.points(3, :);

    x_ = mean(X);
    y_ = mean(Y);
    z_ = mean(Z);

    m = length(X);

    delta = 0;
    M = zeros(3,3);
    for i = 1:m
        delta = delta + (X(i) - x_).^2 + (Y(i) - y_).^2 + (Z(i) - z_).^2;

        M(1, 1) = M(1, 1) + (X(i) - x_).^2;
        M(1, 2) = M(1, 2) + (X(i) - x_)*(Y(i) - y_);
        M(1, 3) = M(1, 3) + (X(i) - x_)*(Z(i) - z_);

        M(2, 1) = M(2, 1) + (Y(i) - y_)*(X(i) - x_);
        M(2, 2) = M(2, 2) + (Y(i) - y_).^2;
        M(2, 3) = M(2, 3) + (Y(i) - y_)*(Z(i) - z_);

        M(3, 1) = M(3, 1) + (Z(i) - z_)*(X(i) - x_);
        M(3, 2) = M(3, 2) + (Z(i) - z_)*(Y(i) - y_);
        M(3, 3) = M(3, 3) + (Z(i) - z_).^2;
    end

    M = delta * eye(3) - M;
    try
        [V, e] = eig(M);
    catch
        continue
    end
    eigs = [e(1,1) e(2,2) e(3,3)];
    [~, minIn] = min(eigs);
    eigVec = V(:, minIn);

    error = eigVec' * M * eigVec;

    if error < tol
        adjustedPoints{j}.points = [curves{j}.points(:, 1), curves{j}.points(:, end)];
    end
end
end
