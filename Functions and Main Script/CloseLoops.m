function loops = CloseLoops(data)

%======================================================%
%
%   CloseLoops takes the curves and finds which ones connect to form loops
%
%%% METHOD %%%
%
%   The distance between all the endpoints are found and if the closest
%   endpoints match (ie endpoint 1's closest endpoint is endpoint 5 and
%   endpoint 5's closest endpoint is endpoint 1), the curves are connected
%   at those endpoints and the direction is preserved through the curves
%   with known direction
%
%%% ARGUMENTS %%%
%
%	 data: Matrix containing the points and the potential at the
%          point. The first column is the x-coordinate, the second column is
%          the y-coordinate, the third column is the z-coordinate, and the
%          fourth column is the potential
%
%%% OUTPUTS %%%
%
%   loops: Cell array where each cell is a 3xN array of points along a
%          curve where N is the number of points along the given loop
%          (don't all need to be the same)
%
%%% Created by: Michael Kline %%%
%======================================================%

points = cell(size(data));
potentials = zeros(size(data));
directions = zeros(size(data));
for i = 1:length(data)
    points{i} = data{i}.points;
    potentials(i) = data{i}.potential;
    if ~isfield(data{i}, 'direction')
        directions(i) = 1;
    end
end

SamePotential = {};
while ~isempty(points)
    idx = find(potentials == potentials(1));
    SamePotential{end+1}.points = points(idx);
    SamePotential{end}.potential = potentials(1);
    SamePotential{end}.directions = directions(idx);
    points(idx) = [];
    potentials(idx) = [];
    directions(idx) = [];
end

loops = {};
for i = 1:length(SamePotential)
    % Find endpoints of each curve
    endpoints = cell2mat(cellfun(@(v)v(:, [1, end]), SamePotential{i}.points, 'UniformOutput', false));
    
    % Find the distance between all of the endpoints
    dists = dist(endpoints);
    dists = dists+max(dists, [], 'all')*eye(length(endpoints));

    % Find the nearest endpoint
    [~, nearestEndpoint] = min(dists);
    
    connections = {};
    for j = 1:length(nearestEndpoint)
        if nearestEndpoint(nearestEndpoint(j)) == j
            connections{end+1} = sort([j, nearestEndpoint(j)]);
        end
    end
    connections = num2cell(unique(cell2mat(connections'), 'rows'), 2)';
    
    curveConnections = {};
    while ~isempty(connections)
        
        idx = 1;
        p1 = connections{1}(1);
        if rem(p1, 2)
            curveConnections{end+1} = -ceil(p1 / 2);
        else
            curveConnections{end+1} = ceil(p1 / 2);
        end
        
        while ~isempty(idx)
            p2 = connections{idx}(find(connections{idx} ~= p1));
            
            connections(idx) = [];
            if rem(p2, 2) % p2 is odd
                curveConnections{end} = [curveConnections{end}, ceil(p2 / 2)];
                p1 = p2 + 1;
            else % p2 is even
                curveConnections{end} = [curveConnections{end}, -ceil(p2 / 2)];
                p1 = p2 - 1;
            end
            
            idx = ceil(find([connections{:}] == p1) / 2);
        end
        curveConnections{end} = curveConnections{end}(1:end-1);
    end
    
    for j = 1:length(curveConnections)
%     disp(curveConnections{j});
        if isempty(find(curveConnections{j} > 0))
            curveConnections{j} = -fliplr(curveConnections{j});
        end
        flip = false;
        loops{end+1} = [];
        for k = 1:length(curveConnections{j})
            if curveConnections{j}(k) > 0
                loops{end} = [loops{end}, SamePotential{i}.points{curveConnections{j}(k)}];
            else % Flip curve when adding it
                loops{end} = [loops{end}, fliplr(SamePotential{i}.points{abs(curveConnections{j}(k))})];
                if SamePotential{i}.directions(abs(curveConnections{j}(k))) % If direction should be correct
                    flip = true; % Flip whole curve
                end
            end
        end
        
        if flip
            loops{end} = fliplr(loops{end});
        end
    end
    
end

end