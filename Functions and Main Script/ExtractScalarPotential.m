function [data, model] = ExtractScalarPotential(model_file, plot_group)

%======================================================%
%
%   ExtractScalarPotential takes a COMSOL model and outputs the value of
%       the magnetic scalar potential along the boundary
%
%%% ARGUMENTS %%%
%
%   model_file: Name of the file to extract scalar potential from
%
%   plot_group: ID of the plot group in the COMSOL model to take the
%               scalar potential from
%
%%% OUTPUTS %%%
%
%         data: Matrix containing the points and the potential at the
%               point. The first column is the x-coordinate, the second column is
%               the y-coordinate, the third column is the z-coordinate, and the
%               fourth column is the potential
%
%        model: Object containing the COMSOL model
%
%%% Created by: Michael Kline %%%
%======================================================%

figure(1),clf
model = mphopen(model_file);
try
    rawData = mphplot(model, plot_group);
    
    data = {};
    for i = 1:length(rawData)
        for j = 1:length(rawData{i})
            if strcmp(rawData{i}{j}.plottype, 'Surface')
                data{end+1} = rawData{i}{j}.p;
                data{end} = [data{end}' rawData{i}{j}.d];
                data{end}(any(isnan(data{end}), 2), :) = [];
            end
        end
    end
catch
    fprintf(2, "ERROR: Failed to extract scalar potential from model\n");
end
end