function finalLoops = ReplaceDuplicatePoints(loops, minDist)

%======================================================%
%
%   ReplaceDuplicatePoints goes from point-to-point and if the distance is
%       greater than the minimum allowed distance, it replaces the two points
%       with their mean
%
%%% METHOD %%%
%
%   If two consecutive points are within minDist of each other, they are
%   replaced by the mean of the two points
%
%%% ARGUMENTS %%%
%
%        loops: Cell array where each cell is a matrix containing the points
%               along each loop
%
%      minDist: Minimum distance between points
%
%%% OUTPUTS %%%
%
%   finalLoops: Cell array where each cell is a matrix containing the points
%               along each loop
%
%%% Created by: Michael Kline %%%
%======================================================%

for i = 1:length(loops)
    % Check Loop Has Enough Points
    if size(loops{i}, 2) < 3
        loops{i} = [];
    else
        dist = zeros(1, size(loops{i}, 2));
        for j = 1:length(dist) - 1
            dist(j) = pdist(loops{i}(:, j:j+1)', 'euclidean');
        end
        dist(end) = pdist([loops{i}(:, end) loops{i}(:, 1)]', 'euclidean');

        if dist(end) < minDist
            loops{i} = [mean([loops{i}(:, end) loops{i}(:, 1)], 2), loops{i}(:, 2:end-1)];
            dist = dist(1:end-1);
        end

        for j = length(dist)-1:-1:1
            if dist(j) < minDist
                if j < length(dist) - 1 && j > 1
                    loops{i} = [loops{i}(:, 1:j-1), mean([loops{i}(:, j) loops{i}(:, j+1)], 2), loops{i}(:, j+2:end)];
                    dist(j-1) = pdist(loops{i}(:, j-1:j)', 'euclidean');
                elseif j == 1
                    if size(loops{i}, 2) > 3
                        loops{i} = [mean([loops{i}(:, j) loops{i}(:, j+1)], 2), loops{i}(:, j+2:end)];
                    else
                        loops{i} = mean([loops{i}(:, j) loops{i}(:, j+1)], 2);
                    end
                else
                    loops{i} = [loops{i}(:, 1:j-1), mean([loops{i}(:, j) loops{i}(:, j+1)], 2)];
                    dist(j-1) = pdist(loops{i}(:, j-1:j)', 'euclidean');
                end
            end
        end
    end
end

% Remove Empty Cells
for i = length(loops):-1:1
    if size(loops{i}, 2) < 3
        loops(i) = [];
    end
end

finalLoops = loops;

end