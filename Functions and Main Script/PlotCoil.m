function PlotCoil(points, options, closeCurve, unique_colors)

%======================================================%
%
%   PlotCoil plots the curves defined in 'points' on one plot
%
%%% METHOD %%%
%
%   This works whether each cell in 'points' is a 3xN array of points where N is the
%   number of points or whether each cell in 'points' is a struct with
%   field named 'points' containing the 3xN array
%
%%% ARGUMENTS
%
%          points: Cell array containing the points along each curve
%
%         options: Character string of normal plot commands. Type 'help
%                  plot' to see full list of available characters
%
%      closeCurve: Set to true to add first point to end of each curve
%                  (default: true)
%
%   unique_colors: Set to true to make each curve a unique color
%                  (default: false)
%
%%% Created by: Michael Kline %%%
%======================================================%

if nargin < 4 || isempty(unique_colors)
    unique_colors = false;
end

if nargin < 3 || isempty(closeCurve)
    closeCurve = true;
end

if nargin < 2 || isempty(options) 
    options = '-';
end

if unique_colors
    cmap = colormap(jet(length(points)));
end

hold on
for j = 1:length(points)
    try
        if unique_colors
            if closeCurve
                plot3([points{j}(1,:) points{j}(1,1)], [points{j}(2,:) points{j}(2,1)], [points{j}(3,:) points{j}(3,1)], options, 'LineWidth', 2, 'Color', cmap(j,:));
            else
                plot3(points{j}(1,:), points{j}(2,:), points{j}(3,:), options, 'LineWidth', 2, 'Color', cmap(j,:));
            end
        else
            if closeCurve
                plot3([points{j}(1,:) points{j}(1,1)], [points{j}(2,:) points{j}(2,1)], [points{j}(3,:) points{j}(3,1)], options, 'LineWidth', 2);
            else
                plot3(points{j}(1,:), points{j}(2,:), points{j}(3,:), options, 'LineWidth', 2);
            end
        end
    catch
        if unique_colors
            if closeCurve
                plot3([points{j}.points(1,:) points{j}.points(1,1)], [points{j}.points(2,:) points{j}.points(2,1)], [points{j}.points(3,:) points{j}.points(3,1)], options, 'LineWidth', 2, 'Color', cmap(j,:));
            else
                plot3(points{j}.points(1,:), points{j}.points(2,:), points{j}.points(3,:), options, 'LineWidth', 2, 'Color', cmap(j,:));
            end
        else
            if closeCurve
                plot3([points{j}.points(1,:) points{j}.points(1,1)], [points{j}.points(2,:) points{j}.points(2,1)], [points{j}.points(3,:) points{j}.points(3,1)], options, 'LineWidth', 2);
            else
                plot3(points{j}.points(1,:), points{j}.points(2,:), points{j}.points(3,:), options, 'LineWidth', 2);
            end
        end
    end
end
hold off
xlabel('X'); ylabel('Y'); zlabel('Z');

end