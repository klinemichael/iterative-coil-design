function PlotCurrentDirection(loops)

%======================================================%
%
%   PlotCurrentDirection plots curves and arrows following the direction of
%   the points along the curve
%
%%% METHOD %%%
%
%   The direction at each point is found by the following point minus the
%   current point, and is plotted using quiver3
%
%%% ARGUMENTS %%%
%
%   loops: Cell array where each cell is a 3xN array of points along a
%          curve where N is the number of points along the curve
%
%%% Created by: Michael Kline %%%
%======================================================%

newLoops = cell(size(loops));
for i = 1:length(loops)
    newLoops{i}(:, end+1) = loops{i}(:, 1);
end

figure,clf
hold on
for i = 1:length(loops)
    points = loops{i};
    newPoints = [points, points(:, 1)];

    vectors = zeros(size(points));
    for j = 1:size(points, 2)
        vectors(:, j) = newPoints(:, j+1) - newPoints(:, j);
    end

    quiver3(points(1, :), points(2, :), points(3, :), vectors(1, :), vectors(2, :), vectors(3, :));
    plot3(newPoints(1, :), newPoints(2, :), newPoints(3, :), 'k');
end
hold off
xlabel('X'); ylabel('Y'); zlabel('Z');

end