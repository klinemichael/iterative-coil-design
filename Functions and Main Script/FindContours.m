function curve = FindContours(data, centerPoint, innerRadius, outerRadius, height, endcaps, numContours, constantU)

%======================================================%
%
%   FindContours takes the value of the scalar potential and finds the
%       equipotential curves
%
%%% METHOD %%%
%
%   The points are sorted by which face of the cylindrical coil they fall
%   on. Then, they are transformed into a 2-D coordinate system where the
%   function FollowEquipotential is called to follow the equipotential
%   curve. The curve is then transformed back into the original 3-D
%   coordinate system.
%
%%% ARGUMENTS %%%
%
%          data: Matrix containing the points and the potential at the
%                point. The first column is the x-coordinate, the second column is
%                the y-coordinate, the third column is the z-coordinate, and the
%                fourth column is the potential
%
%   centerPoint: 1x3 array containing the center point of the coil
%
%   innerRadius: Inner radius of the coil
%
%   outerRadius: Outer radius of the coil, set to 0 for a single-wound coil
%
%        height: Height of the coil
%
%       endcaps: 1 if the coil has endcaps, 0 for no endcaps
%
%      distStep: Distance between points along the equipotential
%
%   numContours: Number of equipotential curves
%
%     constantU: 1x3 array where value is 1 if plane has uniform scalar
%                potential and 0 otherwise. Index order is 'xy', 'yz',
%                'zx'
%
%%% OUTPUTS %%%
%
%         curve: Cell array where each cell is a struct containing the
%                points along the equipotential and the potential of the curve
%
%%% Created by: Michael Kline %%%
%======================================================%

X = data(:, 1);
Y = data(:, 2);
Z = data(:, 3);
potential = data(:, 4);

% Check if this is outer part of coil
radii = vecnorm(data(:, 1:2)');
minR = min(radii); maxR = max(radii);
outerCoil = false;
if ismembertol(minR, innerRadius, 1e-4) && ismembertol(maxR, outerRadius, 1e-4)
    outerCoil = true;
end

%% Separate Points By Surface
% disp('Finding Surfaces');
top = data(find(ismembertol(Z-centerPoint(3), height/2, 1E-6)), :);
topOct = cell(1, 4);
topOct{1} = top(intersect(find(top(:, 1) >= 0), find(top(:, 2) >= 0)), :);
topOct{2} = top(intersect(find(top(:, 1) <= 0), find(top(:, 2) >= 0)), :);
topOct{3} = top(intersect(find(top(:, 1) <= 0), find(top(:, 2) <= 0)), :);
topOct{4} = top(intersect(find(top(:, 1) >= 0), find(top(:, 2) <= 0)), :);
bottom = data(find(ismembertol(Z-centerPoint(3), -height/2, 1E-6)), :);
bottomOct = cell(1, 4);
bottomOct{1} = bottom(intersect(find(bottom(:, 1) >= 0), find(bottom(:, 2) >= 0)), :);
bottomOct{2} = bottom(intersect(find(bottom(:, 1) <= 0), find(bottom(:, 2) >= 0)), :);
bottomOct{3} = bottom(intersect(find(bottom(:, 1) <= 0), find(bottom(:, 2) <= 0)), :);
bottomOct{4} = bottom(intersect(find(bottom(:, 1) >= 0), find(bottom(:, 2) <= 0)), :);
edgeInner = data(find(ismembertol(sqrt((X-centerPoint(1)).^2+(Y-centerPoint(2)).^2), innerRadius, 1E-6)), :);
edgeInnerOct = cell(1, 8);
edgeInnerOct{1} = edgeInner(intersect(intersect(find(edgeInner(:, 1) >= 0), find(edgeInner(:, 2) >= 0)), find(edgeInner(:, 3) >= 0)), :);
edgeInnerOct{2} = edgeInner(intersect(intersect(find(edgeInner(:, 1) <= 0), find(edgeInner(:, 2) >= 0)), find(edgeInner(:, 3) >= 0)), :);
edgeInnerOct{3} = edgeInner(intersect(intersect(find(edgeInner(:, 1) <= 0), find(edgeInner(:, 2) <= 0)), find(edgeInner(:, 3) >= 0)), :);
edgeInnerOct{4} = edgeInner(intersect(intersect(find(edgeInner(:, 1) >= 0), find(edgeInner(:, 2) <= 0)), find(edgeInner(:, 3) >= 0)), :);
edgeInnerOct{5} = edgeInner(intersect(intersect(find(edgeInner(:, 1) >= 0), find(edgeInner(:, 2) >= 0)), find(edgeInner(:, 3) <= 0)), :);
edgeInnerOct{6} = edgeInner(intersect(intersect(find(edgeInner(:, 1) <= 0), find(edgeInner(:, 2) >= 0)), find(edgeInner(:, 3) <= 0)), :);
edgeInnerOct{7} = edgeInner(intersect(intersect(find(edgeInner(:, 1) <= 0), find(edgeInner(:, 2) <= 0)), find(edgeInner(:, 3) <= 0)), :);
edgeInnerOct{8} = edgeInner(intersect(intersect(find(edgeInner(:, 1) >= 0), find(edgeInner(:, 2) <= 0)), find(edgeInner(:, 3) <= 0)), :);
edgeOuter = data(find(ismembertol(sqrt((X-centerPoint(1)).^2+(Y-centerPoint(2)).^2), outerRadius, 1E-6)), :);
edgeOuterOct = cell(1, 8);
edgeOuterOct{1} = edgeOuter(intersect(intersect(find(edgeOuter(:, 1) >= 0), find(edgeOuter(:, 2) >= 0)), find(edgeOuter(:, 3) >= 0)), :);
edgeOuterOct{2} = edgeOuter(intersect(intersect(find(edgeOuter(:, 1) <= 0), find(edgeOuter(:, 2) >= 0)), find(edgeOuter(:, 3) >= 0)), :);
edgeOuterOct{3} = edgeOuter(intersect(intersect(find(edgeOuter(:, 1) <= 0), find(edgeOuter(:, 2) <= 0)), find(edgeOuter(:, 3) >= 0)), :);
edgeOuterOct{4} = edgeOuter(intersect(intersect(find(edgeOuter(:, 1) >= 0), find(edgeOuter(:, 2) <= 0)), find(edgeOuter(:, 3) >= 0)), :);
edgeOuterOct{5} = edgeOuter(intersect(intersect(find(edgeOuter(:, 1) >= 0), find(edgeOuter(:, 2) >= 0)), find(edgeOuter(:, 3) <= 0)), :);
edgeOuterOct{6} = edgeOuter(intersect(intersect(find(edgeOuter(:, 1) <= 0), find(edgeOuter(:, 2) >= 0)), find(edgeOuter(:, 3) <= 0)), :);
edgeOuterOct{7} = edgeOuter(intersect(intersect(find(edgeOuter(:, 1) <= 0), find(edgeOuter(:, 2) <= 0)), find(edgeOuter(:, 3) <= 0)), :);
edgeOuterOct{8} = edgeOuter(intersect(intersect(find(edgeOuter(:, 1) >= 0), find(edgeOuter(:, 2) <= 0)), find(edgeOuter(:, 3) <= 0)), :);

xEqualsZero = data(find(ismembertol(X-centerPoint(1), 0, 1E-6)), :); meanPotentialx0 = mean(xEqualsZero(:, 4));% stdPotentialx0 = std(xEqualsZero(:, 4));
yEqualsZero = data(find(ismembertol(Y-centerPoint(2), 0, 1E-6)), :); meanPotentialy0 = mean(yEqualsZero(:, 4));% stdPotentialy0 = std(yEqualsZero(:, 4));
zEqualsZero = data(find(ismembertol(Z-centerPoint(3), 0, 1E-6)), :); meanPotentialz0 = mean(zEqualsZero(:, 4));% stdPotentialz0 = std(zEqualsZero(:, 4));

%% Find Values of Scalar Potential to Find Contours
maxU = max(potential);
minU = min(potential);
us = zeros(1, numContours);
spacing = (maxU - minU) / (numContours + 1);
for i = 1:length(us)
    us(i) = minU + i*spacing;
end

allPotentials = potential;

% Flip across yz-plane, x=0
if constantU(2)
    allPotentials = [allPotentials; -potential];
end

% Flip across zx-plane, y=0
if constantU(3)
    allPotentials = [allPotentials; 2*meanPotentialy0 - potential];
end

% Flip across xy-plane, z=0
if constantU(1)
    allPotentials = [allPotentials; 2*meanPotentialz0 - potential];
end

numContours = round(range(allPotentials) / range(potential))*numContours;
us = zeros(1, numContours);
spacing = (max(allPotentials) - min(allPotentials)) / (numContours + 1);
for i = 1:length(us)
    us(i) = min(allPotentials) + i*spacing;
end
us = intersect(us(find(us < maxU)), us(find(us > minU)));

%% Project points onto plane
% disp('Projecting Points to Plane');
% Inner Edge
edgeInnerTransformed = cell(size(edgeInnerOct));
for j = 1:length(edgeInnerTransformed)
    edgeInnerTransformed{j} = zeros(size(edgeInnerOct{j}, 1), 3);
    for i = 1:size(edgeInnerOct{j}, 1)
        x = edgeInnerOct{j}(i, 1) - centerPoint(1);
        y = edgeInnerOct{j}(i, 2) - centerPoint(2);
        z = edgeInnerOct{j}(i, 3) - centerPoint(3);

        angle = atan2(y, x);
        if ismember(j, [3 4 7 8]) && angle <= 0
            angle = angle + 2*pi;
        end

        edgeInnerTransformed{j}(i, 1) = angle;
        edgeInnerTransformed{j}(i, 2) = z;
        edgeInnerTransformed{j}(i, 3) = edgeInnerOct{j}(i, 4);
    end
end

% Outer Edge
edgeOuterTransformed = cell(size(edgeOuterOct));
for j = 1:length(edgeOuterTransformed)
    edgeOuterTransformed{j} = zeros(size(edgeOuterOct{j}, 1), 3);
    for i = 1:size(edgeOuterOct{j}, 1)
        x = edgeOuterOct{j}(i, 1) - centerPoint(1);
        y = edgeOuterOct{j}(i, 2) - centerPoint(2);
        z = edgeOuterOct{j}(i, 3) - centerPoint(3);

        angle = atan2(y, x);
        if ismember(j, [3 4 7 8]) && angle <= 0
            angle = angle + 2*pi;
        end

        edgeOuterTransformed{j}(i, 1) = angle;
        edgeOuterTransformed{j}(i, 2) = z;
        edgeOuterTransformed{j}(i, 3) = edgeOuterOct{j}(i, 4);
    end
end

% Top
topTransformed = cell(size(topOct));
for j = 1:length(topTransformed)
    topTransformed{j} = zeros(size(topOct{j}, 1), 3);
    for i = 1:size(topOct{j}, 1)
        topTransformed{j}(i, 1) = topOct{j}(i, 1) - centerPoint(1);
        topTransformed{j}(i, 2) = topOct{j}(i, 2) - centerPoint(2);
        topTransformed{j}(i, 3) = topOct{j}(i, 4);
    end
end

% Bottom
bottomTransformed = cell(size(bottomOct));
for j = 1:length(bottomTransformed)
    bottomTransformed{j} = zeros(size(bottomOct{j}, 1), 3);
    for i = 1:size(bottomOct{j}, 1)
        bottomTransformed{j}(i, 1) = bottomOct{j}(i, 1) - centerPoint(1);
        bottomTransformed{j}(i, 2) = bottomOct{j}(i, 2) - centerPoint(2);
        bottomTransformed{j}(i, 3) = bottomOct{j}(i, 4);
    end
end

%% Find Equipotentials
% disp('Finding Equipotentials');
curve = {};

% Inner Edge
for j = 1:length(edgeInnerTransformed)
    if ~isempty(edgeInnerTransformed{j}) && range(edgeInnerTransformed{j}(:, 1)) && range(edgeInnerTransformed{j}(:, 2)) && range(edgeInnerTransformed{j}(:, 3)) && range(vecnorm(edgeInnerTransformed{j}(:, 1:2)')) > 1e-6
        [curveInnerTransformed, potentials] = FollowEquipotential(edgeInnerTransformed{j}, us);

        % Transform Curve Back
        curveInner = cell(size(curveInnerTransformed));
        for i = 1:length(curveInnerTransformed)
            curveInner{i}.points = zeros(3, size(curveInnerTransformed{i}, 2));
            curveInner{i}.potential = potentials(i);
            for k = 1:size(curveInnerTransformed{i}, 2)
                curveInner{i}.points(1, k) = innerRadius * cos(curveInnerTransformed{i}(1, k)) + centerPoint(1);
                curveInner{i}.points(2, k) = innerRadius * sin(curveInnerTransformed{i}(1, k)) + centerPoint(2);
                curveInner{i}.points(3, k) = curveInnerTransformed{i}(2, k) + centerPoint(3);
            end
            if ismember(j, [5 6 7 8]) % Flip sign on z
                curveInner{i}.points(3, :) = -curveInner{i}.points(3, :);
            end
        end
        
        if outerCoil
            for i = 1:length(curveInner)
                curveInner{i}.points = fliplr(curveInner{i}.points);
            end
        end
        curve = [curve, curveInner];
    end
end

% Outer Edge
for j = 1:length(edgeOuterTransformed)
    if ~isempty(edgeOuterTransformed{j}) && range(edgeOuterTransformed{j}(:, 1)) && range(edgeOuterTransformed{j}(:, 2)) && range(edgeOuterTransformed{j}(:, 3)) && range(vecnorm(edgeOuterTransformed{j}(:, 1:2)')) > 1e-6
        [curveOuterTransformed, potentials] = FollowEquipotential(edgeOuterTransformed{j}, us);

        % Transform Curve Back
        curveOuter = cell(size(curveOuterTransformed));
        for i = 1:length(curveOuterTransformed)
            curveOuter{i}.points = zeros(3, size(curveOuterTransformed{i}, 2));
            curveOuter{i}.potential = potentials(i);
            for k = 1:size(curveOuterTransformed{i}, 2)
                curveOuter{i}.points(1, k) = outerRadius * cos(curveOuterTransformed{i}(1, k)) + centerPoint(1);
                curveOuter{i}.points(2, k) = outerRadius * sin(curveOuterTransformed{i}(1, k)) + centerPoint(2);
                curveOuter{i}.points(3, k) = curveOuterTransformed{i}(2, k) + centerPoint(3);
            end
            if ismember(j, [5 6 7 8]) % Flip sign on z
                curveOuter{i}.points(3, :) = -curveOuter{i}.points(3, :);
            end
        end
        
        curve = [curve, curveOuter];
    end
end

% Top
for j = 1:length(topTransformed)
    if ~isempty(topTransformed{j}) && range(topTransformed{j}(:, 1)) && range(topTransformed{j}(:, 2)) && range(topTransformed{j}(:, 3)) && range(vecnorm(topTransformed{j}(:, 1:2)')) > 1e-6

        % Check if face is outer part of coil, if so do another transformation
        radii = vecnorm(topTransformed{j}(:, 1:2)');
        minR = min(radii); maxR = max(radii);
        outerFace = false;
        if ismembertol(minR, innerRadius, 1e-4) && ismembertol(maxR, outerRadius, 1e-4)
            outerFace = true;
            for i = 1:length(radii)
                angle = atan2(topTransformed{j}(i, 2), topTransformed{j}(i, 1));
                if ismember(j, [3 4]) && angle <= 0
                    angle = angle + 2*pi;
                end
                topTransformed{j}(i, 1) = angle;
                topTransformed{j}(i, 2) = radii(i) - innerRadius;
            end
        end

        [curveTopTransformed, potentials] = FollowEquipotential(topTransformed{j}, us);

        % Transform Curve Back
        if outerFace
            for i = 1:length(curveTopTransformed)
                for k = 1:size(curveTopTransformed{i}, 2)
                    curveTopTransformed{i}(1:2, k) = (curveTopTransformed{i}(2, k) + innerRadius) * [cos(curveTopTransformed{i}(1, k)); sin(curveTopTransformed{i}(1, k))];
                end
            end
        end

        curveTop = cell(size(curveTopTransformed));
        for i = 1:length(curveTopTransformed)
            curveTop{i}.points = zeros(3, size(curveTopTransformed{i}, 2));
            curveTop{i}.potential = potentials(i);
            for k = 1:size(curveTopTransformed{i}, 2)
                curveTop{i}.points(1, k) = curveTopTransformed{i}(1, k) + centerPoint(1);
                curveTop{i}.points(2, k) = curveTopTransformed{i}(2, k) + centerPoint(2);
                curveTop{i}.points(3, k) = height/2 + centerPoint(3);
            end
            if ismember(j, [2 3]) && ~outerFace % Flip sign on x
                curveTop{i}.points(1, :) = -curveTop{i}.points(1, :);
            end
            if ismember(j, [3 4]) && ~outerFace % Flip sign on y
                curveTop{i}.points(2, :) = -curveTop{i}.points(2, :);
            end
        end
        
        if outerCoil
            for i = 1:length(curveTop)
                curveTop{i}.points = fliplr(curveTop{i}.points);
            end
        end
        curve = [curve, curveTop];
    end
end

% Bottom
for j = 1:length(bottomTransformed)
    if ~isempty(bottomTransformed{j}) && range(bottomTransformed{j}(:, 1)) && range(bottomTransformed{j}(:, 2)) && range(bottomTransformed{j}(:, 3)) && range(vecnorm(bottomTransformed{j}(:, 1:2)')) > 1e-6

        % Check if face is outer part of coil, if so do another transformation
        radii = vecnorm(bottomTransformed{j}(:, 1:2)');
        minR = min(radii); maxR = max(radii);
        outerFace = false;
        if ismembertol(minR, innerRadius, 1e-4) && ismembertol(maxR, outerRadius, 1e-4)
            outerFace = true;
            for i = 1:length(radii)
                angle = atan2(bottomTransformed{j}(i, 2), bottomTransformed{j}(i, 1));
                if ismember(j, [3 4]) && angle <= 0
                    angle = angle + 2*pi;
                end
                bottomTransformed{j}(i, 1) = angle;
                bottomTransformed{j}(i, 2) = radii(i) - innerRadius;
            end
        end

        [curveBottomTransformed, potentials] = FollowEquipotential(bottomTransformed{j}, us);

        % Transform Curve Back
        if outerFace
            for i = 1:length(curveBottomTransformed)
                for k = 1:size(curveBottomTransformed{i}, 2)
                    curveBottomTransformed{i}(1:2, k) = (curveBottomTransformed{i}(2, k) + innerRadius) * [cos(curveBottomTransformed{i}(1, k)); sin(curveBottomTransformed{i}(1, k))];
                end
            end
        end

        curveBottom = cell(size(curveBottomTransformed));
        for i = 1:length(curveBottomTransformed)
            curveBottom{i}.points = zeros(3, size(curveBottomTransformed{i}, 2));
            curveBottom{i}.potential = potentials(i);
            for k = 1:size(curveBottomTransformed{i}, 2)
                curveBottom{i}.points(1, k) = curveBottomTransformed{i}(1, k) + centerPoint(1);
                curveBottom{i}.points(2, k) = curveBottomTransformed{i}(2, k) + centerPoint(2);
                curveBottom{i}.points(3, k) = height/2 + centerPoint(3);
            end
            if ismember(j, [2 3]) && ~outerFace % Flip sign on x
                curveBottom{i}.points(1, :) = -curveBottom{i}.points(1, :);
            end
            if ismember(j, [3 4]) && ~outerFace % Flip sign on y
                curveBottom{i}.points(2, :) = -curveBottom{i}.points(2, :);
            end
            curveBottom{i}.points(3, :) = -curveBottom{i}.points(3, :);
        end
        
        if outerCoil
            for i = 1:length(curveBottom)
                curveBottom{i}.points = fliplr(curveBottom{i}.points);
            end
        end
        curve = [curve, curveBottom];
    end
end

%% Remove Empty Cells from Curve
for i = length(curve):-1:1
    if isempty(curve{i}.points)
        curve(i) = [];
    end
end

%% Snap Curve to Geometry
curve = FollowGeometry(curve, centerPoint, innerRadius, outerRadius, height, endcaps);

end