function smoothedPoints = SmoothingSpline(adjustedPoints, p, minDist)

%======================================================%
%
%   SmoothingSpline takes a set of points forming a curve and finds a
%       smoothing spline between the points
%
%%% METHOD %%%
%
%   Since MATLAB's smoothing spline only works for curves in 2-D, each
%   curve is separated into 3 by each dimension being the y-coordinate and
%   its index being the x-coordinate. The y-coordinates along the curve are
%   then merged back to form one 3-D curve. Then, a natural cubic spline is
%   fit to the points and new points are found at roughly minDist along the
%   curve
%
%%% ARGUMENTS %%%
%
%   adjustedPoints: cell array where each cell is a struct that contains
%                   the points of each curve and the potential of the curve
%
%                p: smoothing parameter, 0 is linear regression, 1 is
%                   natural cubic spline
%
%          minDist: minimum distance between points
%
%%% OUTPUTS %%%
%
%   smoothedPoints: cell array where each cell is a struct that contains
%                   the points of each curve, after being smoothed by a
%                   smoothing spline, and the potential of the curve
%
%%% Created by: Michael Kline %%%
%======================================================%


if nargin<2||isempty(p), p = -1; end

smoothedPoints = {};
for j = 1:length(adjustedPoints) 
    [ndim, npts] = size(adjustedPoints{j}.points);
    if npts > 2
        % Create a smoothing spline for each curve
        smoothedPoints{j}.points = zeros(size(adjustedPoints{j}.points));
        smoothedPoints{j}.potential = adjustedPoints{j}.potential;
        for k = 1:ndim
           smoothedPoints{j}.points(k,:) = ppval(csaps(1:npts, double(adjustedPoints{j}.points(k,:)), p), 1:npts);
        end
        spline = cscvn(smoothedPoints{j}.points);
        
        % Approximate arclength along curve to get scale for distance from point-to-point
        breaks = spline.breaks;
        dl = (max(breaks)-min(breaks))/100;
        points = [];
        for i = min(breaks):dl:max(breaks)
            points(:, end+1) = fnval(spline, i);
        end
        arclength = 0;
        for i = 1:size(points, 2)-1
            arclength = arclength + pdist(points(:, i:i+1)', 'euclidean');
        end

        % Add points roughly every minDist along spline (some will be further, some will be shorter)
        numIntervals = floor(arclength / minDist);
        intervalSize = (max(breaks)-min(breaks)) / numIntervals;
        smoothedPoints{j}.points = [];
        for i = 0:numIntervals
           smoothedPoints{j}.points(:, i + 1) = fnval(spline, i*intervalSize);
        end

    else
        smoothedPoints{j}.points = double(adjustedPoints{j}.points);
        smoothedPoints{j}.potential = adjustedPoints{j}.potential;
    end
end

end