function hasCorners = HasCorners(points)

angles = pi * ones(size(points, 2) - 1, 1);
for i = 2:size(points, 2) - 1
    angles(i) = acos(((points(:, i - 1) - points(:, i)) / norm(points(:, i - 1) - points(:, i)))' * ((points(:, i + 1) - points(:, i)) / norm(points(:, i + 1) - points(:, i))));
end
angles(1) = acos(((points(:, end - 1) - points(:, 1)) / norm(points(:, end - 1) - points(:, 1)))' * ((points(:, 2) - points(:, 1)) / norm(points(:, 2) - points(:, 1))));

if isempty(find(angles < 2 * pi / 3, 1))
    hasCorners = false;
else
    hasCorners = true;
end

end