function contour = LoadPreSortedContours(contour_file)

sortedContours = csvread(contour_file);

contour = {};
numCurves = length(unique(sortedContours(:,5)));
for i = 1:numCurves
    idx = find(sortedContours(:,5) == i);
    contour{i}.points = sortedContours(idx, 1:3)';
    contour{i}.potential = sortedContours(idx(1), 4);
end

end