function keyEvent(h, ~)

key = get(h, 'CurrentKey');
global curveIdx;
global pointsArr;
global pointIdx;

if key == 'm' %% Merge Selected Curves
    curves = find(curveIdx==1);
    points = [];
    for i = 1:length(curves)
        points = [points, pointsArr{curves(i)}];
    end
    
    pointsArr(curves) = []; % Remove cells to be merged
    
    userConfig = struct('xy',[transpose(points(1,:)) transpose(points(2,:)) transpose(points(3,:))], 'showProg', false, 'showResult', false);
    resultStruct = tsp_nn(userConfig);
    optRoute = resultStruct.optRoute;
    sortedPoints = 0 * points;
    for i = 1:length(optRoute)
        n = optRoute(i);
        sortedPoints(:,i) = points(:,n);
    end
    
    pointsArr{end + 1} = sortedPoints; % Add merged points back
    curveIdx = zeros(1, length(pointsArr));
    
    children = get(gca, 'children');
    delete(children);
    hold on
    for i = 1:length(pointsArr)
        plot3(pointsArr{i}(1,:), pointsArr{i}(2,:), pointsArr{i}(3,:), 'o-', 'LineWidth', 1); 
    end
    hold off
elseif key == 'c' %% Set Selected Point(s) as a Corner
elseif key == 'd' %% Delete Selected Point(s)
    pointCloud = cell2mat(pointsArr);
    pointsToDelete = pointCloud(:, find(pointIdx == 1));
    for i = 1:length(pointsArr)
        for j = 1:size(pointsToDelete, 2)
            pointsArr{i}(:, find(all(bsxfun(@eq, pointsToDelete(:, j)', pointsArr{i}'), 2)' == 1)) = [];
        end
    end
    
    pointIdx = zeros(1, size(cell2mat(pointsArr), 2));
    
    children = get(gca, 'children');
    delete(children);
    hold on
    for i = 1:length(pointsArr)
        plot3(pointsArr{i}(1,:), pointsArr{i}(2,:), pointsArr{i}(3,:), 'o-', 'LineWidth', 1); 
    end
    hold off
elseif key == 'u' %% De-select All
    curveIdx = zeros(1, length(pointsArr));
    pointIdx = zeros(1, size(cell2mat(pointsArr), 2));
    
    children = get(gca, 'children');
    delete(children);
    hold on
    for i = 1:length(pointsArr)
        plot3(pointsArr{i}(1,:), pointsArr{i}(2,:), pointsArr{i}(3,:), 'o-', 'LineWidth', 1); 
    end
    hold off
elseif strcmp(key, 'control')
else
    disp(key);
end

end