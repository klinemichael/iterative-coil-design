function curve = FollowEquipotential(data, us, distStep)

%======================================================%
%
%   FollowEquipotential takes the value of the scalar potential on one 2D surface
%       and finds the equipotential curves
%
%%% METHOD %%%
%
%   A linear interpolation fit is applied to the 2-D points with
%   z-coordinate as the potential. Beginning at y=0 (or x=0 if the
%   equipotential is constant at y=0), a new point is found by the normal
%   cross the gradient N x gradU, and the potential is checked and the
%   point is adjusted to stay at the correct potential. This is repeated
%   until the point falls outside the convex hull, at which point the last
%   point is fixed onto the convex hull and the loop is terminated.
%
%%% ARGUMENTS %%%
%
%       data: Matrix containing the points and the potential at the
%             point. The first column is the x-coordinate, the second column is
%             the y-coordinate, the third column is the z-coordinate, and the
%             fourth column is the potential
%
%         us: Value of each potential to find the equipotential curve
%
%   distStep: Distance between points along the equipotential
%
%%% OUTPUTS %%%
%
%      curve: Cell array where each cell is a struct containing the
%             points along the equipotential and the potential of the curve
%
%%% NOTE %%%
%
%   Commented out is a method of doing the same thing but using
%   'scatteredInterpolant' instead of 'fit'. However, this is slower than
%   using a fit. If using scatteredInterpolant, set interpolation type to 'natural' or
%   'linear' and keep extrapolation type as 'none'
%
%   Pitfalls: This program starts the equipotentials at the x-axis or the y-axis, but
%   if the potential does not connect to either of these axes, it will fail
%   to find it
%
%%% Created by: Michael Kline %%%
%======================================================%

data(:, 1) = abs(data(:, 1));
data(:, 2) = abs(data(:, 2));

minX = min(data(:, 1)); if minX < 1E-6, minX = 0; end
maxX = max(data(:, 1));
minY = min(data(:, 2)); if minY < 1E-6, minY = 0; end
maxY = max(data(:, 2));

data(find(ismembertol(data(:, 1), minX, 1E-6)), 1) = minX;
data(find(ismembertol(data(:, 1), maxX, 1E-6)), 1) = maxX;
data(find(ismembertol(data(:, 2), minY, 1E-6)), 2) = minY;
data(find(ismembertol(data(:, 2), maxY, 1E-6)), 2) = maxY;

% f = scatteredInterpolant(data(:, 1:2), data(:, 3), 'natural', 'none');
f = fit(data(:, 1:2), data(:, 3), 'linearinterp');

curve = cell(1, length(us));
Xs = minX:(maxX-minX)/200:maxX;
Ys = minY:(maxY-minY)/200:maxY;
pointsX = f(Xs, zeros(size(Xs)));
pointsY = f(zeros(size(Ys)), Ys);
maxUx = max(pointsX); minUx = min(pointsX);
maxUy = max(pointsY); minUy = min(pointsY);

% figure,clf
% hold on
% scatter3(data(:, 1), data(:, 2), data(:, 3), [], data(:, 3), '.')

for i = 1:length(us)
    if (us(i) <= maxUx) && (us(i) >= minUx)
        [indices, dists] = knnsearch(pointsX', us(i), 'K', 2);
        startpoint = [(dists(2)*Xs(indices(1)) + dists(1)*Xs(indices(2))) / (dists(1) + dists(2)), 0];
%         ux = gradScatteredInterpolant(f, startpoint(1), startpoint(2));
        ux = differentiate(f, startpoint);
        if ux < 0
            signDir = -1;
        else
            signDir = 1;
        end
    elseif (us(i) <= maxUy) && (us(i) >= minUy)
        [indices, dists] = knnsearch(pointsY', us(i), 'K', 2);
        startpoint = [0, (dists(2)*Ys(indices(1)) + dists(1)*Ys(indices(2))) / (dists(1) + dists(2))];
%         [~, uy] = gradScatteredInterpolant(f, startpoint(1), startpoint(2));
        [~, uy] = differentiate(f, startpoint);
        if uy > 0
            signDir = -1;
        else
            signDir = 1;
        end
    else
        continue
    end

    curve{i} = [];
    curve{i}(:, 1) = startpoint';
    while ~isnan(f(startpoint(1), startpoint(2))) && ((startpoint(1) >= minX) && (startpoint(1) <= maxX) && (startpoint(2) >= minY) && (startpoint(2) <= maxY))

        % Calculate Gradient of Scalar Potential
        u = f(startpoint(1), startpoint(2));
%         [ux, uy] = gradScatteredInterpolant(f, startpoint(1), startpoint(2));
        [ux, uy] = differentiate(f, startpoint);
        if isnan(ux), ux = 0; end
        if isnan(uy), uy = 0; end

        % Fix Point to Equipotential
        gradu = [ux, uy];
        dirU = sign(us(i) - u);
        xs = startpoint(1):dirU*distStep/2*gradu(1):startpoint(1)+dirU*distStep*2*gradu(1);
        ys = startpoint(2):dirU*distStep/2*gradu(2):startpoint(2)+dirU*distStep*2*gradu(2);
        if length(xs) < 1, xs = startpoint(1)*ones(size(ys)); end
        if length(ys) < 1, ys = startpoint(2)*ones(size(xs)); end
        try
            u2 = f(xs, ys);
        catch
            u2 = [];
        end
        if ~isempty(u2) && (us(i) <= max(u2)) && (us(i) >= min(u2))
            [indices, dists] = knnsearch(u2', us(i), 'K', 2);
            startpoint = (dists(2)*[xs(indices(1)), ys(indices(1))] + dists(1)*[xs(indices(2)), ys(indices(2))]) / (dists(1) + dists(2));
            curve{i}(:, end) = startpoint';
%             [ux, uy] = gradScatteredInterpolant(f, startpoint(1), startpoint(2));
            [ux, uy] = differentiate(f, startpoint);
            if isnan(ux), ux = 0; end
            if isnan(uy), uy = 0; end
        end

        dirEquipotential = cross([0, 0, 1], [ux, uy, 0]);
        dirEquipotential = dirEquipotential(1:2) / norm(dirEquipotential(1:2));

        startpoint = startpoint + signDir*distStep*dirEquipotential;
        curve{i}(:, end+1) = startpoint';
% plot3(startpoint(1), startpoint(2), f(startpoint(1), startpoint(2)), 'k.');
    end
    
    % Make sure last point lies within the convex hull
    k = convhull(data(:, 1), data(:, 2));
    IN = inpolygon(startpoint(1), startpoint(2), data(k, 1), data(k, 2));
    if ~IN
        outerPoints = data(k, 1:2);
        indices = knnsearch(outerPoints, startpoint, 'K', 2);
        indices = sort(indices);
        if indices(2) - indices(1) == 1
            p1 = outerPoints(indices(1), :);
            p2 = outerPoints(indices(2), :);
            startpoint = project_point_to_line_segment(p1, p2, startpoint);
            curve{i}(:, end) = startpoint';
        else
            fprintf(2, 'WARNING: Point outside of convex hull\n');
        end
    end
    
    if signDir < 0
        curve{i} = fliplr(curve{i});
    end
end

    function [q] = project_point_to_line_segment(A,B,p)
      % returns q the closest point to p on the line segment from A to B 

      % vector from A to B
      AB = (B-A);
      % squared distance from A to B
      AB_squared = dot(AB,AB);
      if(AB_squared == 0)
        % A and B are the same point
        q = A;
      else
        % vector from A to p
        Ap = (p-A);
        % from http://stackoverflow.com/questions/849211/
        % Consider the line extending the segment, parameterized as A + t (B - A)
        % We find projection of point p onto the line. 
        % It falls where t = [(p-A) . (B-A)] / |B-A|^2
        t = dot(Ap,AB)/AB_squared;
        if (t < 0.0) 
          % "Before" A on the line, just return A
          q = A;
        elseif (t > 1.0) 
          % "After" B on the line, just return B
          q = B;
        else
          % projection lines "inbetween" A and B on the line
          q = A + t * AB;
        end
      end
    end

%     function [ux, uy] = gradScatteredInterpolant(interpolant, x, y)
%         epsilon = 1E-12;
%         
%         ux = (interpolant(x+epsilon, y) - interpolant(x, y)) / epsilon;
%         uy = (interpolant(x, y+epsilon) - interpolant(x, y)) / epsilon;
%         ux = ux / norm([ux, uy]);
%         uy = uy / norm([ux, uy]);
%     end

% hold off
end