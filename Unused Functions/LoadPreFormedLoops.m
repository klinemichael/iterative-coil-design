function finalLoops = LoadPreFormedLoops(loop_file)

loops = csvread(loop_file);

finalLoops = {};
numLoops = length(unique(loops(:,4)));
for i = 1:numLoops
    idx = find(loops(:,4) == i);
    finalLoops{i} = loops(idx, 1:3)';
end

end