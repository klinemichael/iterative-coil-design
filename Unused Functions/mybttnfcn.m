function mybttnfcn(h, ~)

modifiers = get(h,'currentModifier');
ctrlIsPressed = ismember('control', modifiers);
global pointsArr;
global curveIdx;
global pointIdx;
pointCloud = cell2mat(pointsArr);
point = get(gca, 'CurrentPoint'); % mouse click position
camPos = get(gca, 'CameraPosition'); % camera position
camTgt = get(gca, 'CameraTarget'); % where the camera is pointing to
camDir = camPos - camTgt; % camera direction
camUpVect = get(gca, 'CameraUpVector'); % camera 'up' vector
% build an orthonormal frame based on the viewing direction and the 
% up vector (the "view frame")
zAxis = camDir/norm(camDir);    
upAxis = camUpVect/norm(camUpVect); 
xAxis = cross(upAxis, zAxis);
yAxis = cross(zAxis, xAxis);
rot = [xAxis; yAxis; zAxis]; % view rotation 
% the point cloud represented in the view frame
rotatedPointCloud = rot * pointCloud; 
% the clicked point represented in the view frame
rotatedPointFront = rot * point' ;
% find the nearest neighbour to the clicked point 
pointCloudIndex = dsearchn(rotatedPointCloud(1:2,:)', rotatedPointFront(1:2));
h = findobj(gca,'Tag','pt'); % try to find the old points
selectedPoint = pointCloud(:, pointCloudIndex);

if ctrlIsPressed %% Select Point on Ctrl+Click
    curveIdx = 0 * curveIdx;
    
    if pointIdx(pointCloudIndex) == 1
        pointIdx(pointCloudIndex) = 0;
    else
        pointIdx(pointCloudIndex) = 1;
    end
    
    % Plot Selected Points
    idx = find(pointIdx == 1);
    children = get(gca, 'children');
    delete(children);
    hold on
    for i = 1:length(pointsArr)
          h = plot3(pointsArr{i}(1,:), pointsArr{i}(2,:), pointsArr{i}(3,:), 'o-', 'LineWidth', 1);
    end
    for i = 1:length(idx)
        h = plot3(pointCloud(1, idx(i)), pointCloud(2, idx(i)), pointCloud(3, idx(i)), 'r.-', 'MarkerSize', 30); 
    end
    hold off
    set(h,'Tag','pt');  % set its Tag property for later use
    
else %% Select Curve on Click
    pointIdx = 0 * pointIdx;
    
    % Find What Curve The Point is In
    idx = zeros(1, length(pointsArr));
    for i = 1:length(idx)
        for j = 1:size(pointsArr{i}, 2)
            if ismember(selectedPoint', pointsArr{i}(:,j)', 'rows')
                idx(i) = 1;
            end
        end
    end
    curveIdx = xor(curveIdx, idx); % Remove or add curves as needed

    % Plot Curves
    children = get(gca, 'children');
    delete(children);
    hold on
    for i = 1:length(pointsArr)
        if curveIdx(i) == 1
           h = plot3(pointsArr{i}(1,:), pointsArr{i}(2,:), pointsArr{i}(3,:), 'r.-', 'MarkerSize', 30, 'LineWidth', 5);
        else
            h = plot3(pointsArr{i}(1,:), pointsArr{i}(2,:), pointsArr{i}(3,:), 'o-', 'LineWidth', 1);
        end
    end
    hold off
    set(h,'Tag','pt');  % set its Tag property for later use
end