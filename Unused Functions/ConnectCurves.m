function connectedCurves = ConnectCurves(points, connections)
connectedCurves = cell(1, length(connections));
for i = 1:length(connections)
    connectedCurves{i}.points = [];
    for j = 1:length(connections{i})
        connectedCurves{i}.points = [connectedCurves{i}.points, points{connections{i}(j)}.points];
    end
    connectedCurves{i}.potential = points{connections{i}(1)}.potential;
end
end