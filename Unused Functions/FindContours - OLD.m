% function contours = FindContours(model, plot_group)

% data = mphplot(model, plot_group);
tic
data = csvread('C:/Users/Michael/Desktop/CreateCoil/scalarpotential1.csv', 8);
distStep = 0.002;
numSteps = 200;
numContours = 4;

centerPoint = [0, 0, 0];
innerRadius = 5 * 0.0254;
outerRadius = 7 * 0.0254; % 0 for single-wound
height = 8 * 0.0254;
endcaps = 1; % 0 for no endcaps, 1 for endcaps

X = data(:, 1);
Y = data(:, 2);
Z = data(:, 3);
potential = data(:, 4);

maxU = max(potential);
minU = min(potential);
spacing = (maxU - minU) / (numContours + 1);

startpoints = intersect(find(X==0), find(ismembertol(Z, height/2)));

figure(1),clf
hold on
curve = cell(1,numContours);
us = zeros(numContours, numSteps);
for j = 1:numContours
    
    % Find start point
    find_u = minU + j*spacing;
    [indices, dists] = knnsearch(potential(startpoints), find_u, 'K', 2);
    in1 = startpoints(indices(1));
    in2 = startpoints(indices(2));
    startpoint = (dists(2)*[X(in1); Y(in1); Z(in1)] + dists(1)*[X(in2); Y(in2); Z(in2)]) / (dists(1) + dists(2));
    plot3(startpoint(1), startpoint(2), startpoint(3), 'k.', 'MarkerSize', 5);
    
    curve{j} = zeros(3, numSteps+1);
    curve{j}(:, 1) = startpoint;
    
    for i = 1:numSteps

        % Find 3 nearest points (4 because the first 3 might be colinear)
        [indices, dists] = knnsearch(data(:, 1:3), startpoint', 'K', 4);

        A = data(indices(1), 1:3);
        u1 = data(indices(1), 4);

        B = data(indices(2), 1:3);
        u2 = data(indices(2), 4);

        C = data(indices(3), 1:3);
        u3 = data(indices(3), 4);

        D = data(indices(4), 1:3);
        u4 = data(indices(4), 4);

        % Create original vectors
        AB = B-A;
        AC = C-A;

        % Check if AB and AC are colinear
        if norm(AB+AC) < 1e-10
            C = D;
            u3 = u4;
            AC = C-A;
        end

        % Find normal vector to plane
        N = cross(AB, AC);
        rlhand = sign(dot(N, [1 1 1]));
        if rlhand < 0
            N = -N;
        end

        % Find new unit vectors for transformation
        U = AB / norm(AB); % First Unit Vector (used for transformation to 2D coordinate system)
        uN = N / norm(N); % Third Unit Vector (used for finding gradient)
        V = cross(U, uN); % Second Unit Vector (used for transformation to 2D coordinate system)

        % Shifted unit vectors
        u=A+U;
        v=A+V;
        n=A+uN;

        % Find transformation matrix
        D = [0 1 0 0; 0 0 1 0; 0 0 0 1; 1 1 1 1];
        S = [A(1) u(1) v(1) n(1); A(2) u(2) v(2) n(2); A(3) u(3) v(3) n(3); 1 1 1 1];
        M = D * pinv(S); % Transformation Matrix from 3D plane to 2D coordinate system

        % Transform point and three nearest points
        p1 = M * [A 1]'; % point A
        p2 = M * [B 1]'; % point B
        p3 = M * [C 1]'; % point C
        p4 = M * [startpoint' 1]'; % original point

        % Create linear interpolation
        P = [1 p1(1) p1(2); 1 p2(1) p2(2); 1 p3(1) p3(2)];
        Q = [u1; u2; u3];
        a = pinv(P) * Q;

        if i > 1
            % Search nearby for point with same equipotential
            b = (us(1)-a(1))/a(3);
            m = -a(2)/a(3);
            x = p4(1)/2 + (p4(2)-b)/(2*m);
            y=1/a(3)*(us(1)-a(1)-a(2)*x);
            p4_2 = [x; y; 0; 1];
            startpoint2 = pinv(M) * p4_2;
            startpoint2 = startpoint2(1:3);

            if pdist([startpoint, startpoint2]', 'euclidean') < 2 * distStep
                p4 = p4_2;
                startpoint = FollowGeometry(startpoint2, centerPoint, innerRadius, outerRadius, height, endcaps);
                curve{j}(:, i) = startpoint;
    %             disp(['point ' num2str(i) ' moved to fit equipotential']);
            end
            
        end

        % Find gradient and direction of equipotential
        u = a(1) + a(2)*p4(1) + a(3)*p4(2);
        us(j, i) = u;
        gradu = pinv(M) * [a(2); a(3); 0; 1];
        gradu = gradu(1:3);
        dirEquipotential = cross(uN, gradu)';
        dirEquipotential = dirEquipotential / norm(dirEquipotential);

        % Find new point
        startpoint = startpoint + distStep * dirEquipotential;

        % Snap new point to geometry
        startpoint = FollowGeometry(startpoint, centerPoint, innerRadius, outerRadius, height, endcaps);

        % Save and plot new point
        curve{j}(:, i+1) = startpoint;
        plot3(startpoint(1), startpoint(2), startpoint(3), 'k.', 'MarkerSize', 5);
            
        if startpoint(3) <= 0 || startpoint(2) <= 0 || startpoint(1) <= 0
            curve{j} = curve{j}(:, 1:i+1);
            break;
        end
    end
end

scatter3(X, Y, Z, [], potential, '.');
plotCoil(curve, 'k', false, false);
hold off
time = toc;
disp(['Elapsed time is ' num2str(floor(time / 60)) 'min and ' num2str(floor(rem(time, 60))) 'sec']); 
% end