function [removedSmallLoops, connections] = RemoveSmallLoops(separatedCorners, symmetry, innerRadius, outerRadius, height, centerPoint, connections)

curves = {};
potentials = zeros(length(separatedCorners));
for i = 1:length(separatedCorners)
    curves{i} = separatedCorners{i}.points;
    potentials(i) = separatedCorners{i}.potential;
end

% If curve doesn't go from x=0,y=0,z=0,cylinder edge to another, then
% delete it
if symmetry == 8
    for i = length(curves):-1:1
        point1 = curves{i}(:, 1) - centerPoint';
        point2 = curves{i}(:, end) - centerPoint';
        
        x1 = point1(1); y1 = point1(2); z1 = point1(3);
        x2 = point2(1); y2 = point2(2); z2 = point2(3);
        r1 = sqrt(x1^2 + y1^2);
        r2 = sqrt(x2^2 + y2^2);
        angle1 = atan(y1/x1);
        angle2 = atan(y2/x2);
        if x1<0
            angle1 = angle1 + pi;
        end
        if x2<0
            angle2 = angle2 + pi;
        end
        
        if abs(r1 - innerRadius) < abs(r1 - outerRadius)
            nearestCorner1 = innerRadius * [cos(angle1), sin(angle1)];
        else
            nearestCorner1 = outerRadius * [cos(angle1), sin(angle1)];
        end
        if abs(r2 - innerRadius) < abs(r2 - outerRadius)
            nearestCorner2 = innerRadius * [cos(angle2), sin(angle2)];
        else
            nearestCorner2 = outerRadius * [cos(angle2), sin(angle2)];
        end
        
        if z1 < 0
            nearestCorner1 = [nearestCorner1, -height/2];
        else
            nearestCorner1 = [nearestCorner1, height/2];
        end
        if z2 < 0
            nearestCorner2 = [nearestCorner2, -height/2];
        else
            nearestCorner2 = [nearestCorner2, height/2];
        end
        
        iscorner1 = pdist([x1, y1, z1; nearestCorner1], 'euclidean') < 1e-3;
        iscorner2 = pdist([x2, y2, z2; nearestCorner2], 'euclidean') < 1e-3;
        
        if (x1==0 || y1==0 || z1==0 || iscorner1) && (x2==0 || y2==0 || z2==0 || iscorner2)
        else
            fprintf(2, 'Removed Extraneous Curve\n');
            curves(i) = [];
            potentials(i) = [];
            for k = 1:length(connections)
                connections{k}(find(connections{k} == i)) = [];
                connections{k}(find(connections{k} > i)) = connections{k}(find(connections{k} > i)) - 1;
            end
        end
    end
end

removedSmallLoops = cell(1, length(curves));
for i = 1:length(curves)
    removedSmallLoops{i}.points = curves{i};
    removedSmallLoops{i}.potential = potentials(i);
end

for i = length(connections):-1:1
    if isempty(connections{i})
        connections(i) = [];
    end
end

% % % % % Go from point to point in each loop and find the two nearest points to it, if
% % % % % it's not the two directly next to it, see if the points in between form a loop.
% % % % % If so, then flag them
% % % % 
% % % % 
% % % % removedSmallLoops = cell(1, length(curves));
% % % % for i = 1:length(curves)
% % % %     dl = zeros(size(curves{i}, 1), size(curves{i}, 2) - 1);
% % % %     vert = zeros(1, size(dl, 2));
% % % %     for j = 1:size(dl, 2)
% % % %         dl(:, j) = curves{i}(:, j+1) - curves{i}(:, j);
% % % %         dl(:, j) = dl(:, j)/norm(dl(:, j));
% % % %         vert(j) = abs([0, 0, 1]*dl(:, j));
% % % %     end
% % % %     isvert = (mean(vert)-0.5) > 0; % 1 for vertical, 0 for horizontal
% % % % 
% % % %     if isvert % Vertical Curve
% % % %         if symmetry == 8
% % % %             % Check for extraneous curve beyond edge
% % % %             bottom = find(curves{i}(3, :) == 0);
% % % %             top = find(curves{i}(3, :) == height/2);
% % % %             removedSmallLoops{i}.bottom = bottom;
% % % %             removedSmallLoops{i}.top = top;
% % % %             if length(bottom) == 1 && length(top) == 1
% % % %                 tempPoints = curves{i}(:, min([bottom, top]:max([bottom, top])));
% % % %                 removedSmallLoops{i}.points = tempPoints;
% % % %             else
% % % %             end
% % % %         else
% % % %         end
% % % %     else % Horizontal Curve
% % % %         removedSmallLoops{i} = 'Horizontal';
% % % %     end
% % % % end


end