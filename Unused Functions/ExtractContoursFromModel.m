function [data, model] = ExtractContoursFromModel(model_file, plot_group)

figure(1),clf
% Extract the data and plot to check
model = mphopen(model_file);
try
    rawData = mphplot(model, plot_group);
    
    data = {};
    for i = 1:length(rawData)
        for j = 1:length(rawData{i})
            if strcmp(rawData{i}{j}.plottype, 'Contour')
                data{end+1}.points = rawData{i}{j}.p;
                data{end}.potential = rawData{i}{j}.d;
                data{end}.potential = data{end}.potential(1);
            end
        end
    end
catch
    fprintf(2, "ERROR: Could not get points from model, using saved data instead\n");
    rawData = csvread('C:\Users\Michael\Desktop\Extract Contours\Contours.csv');
    data = {};
    numEquipotentials = length(unique(rawData(:,4)));
    for j = 1:numEquipotentials
        equipotentialLevel = rawData(1,4);
        index = find(rawData(:,4) == equipotentialLevel);
        data{j}.points = rawData(1:length(index), 1:3)';
        data{j}.potential = rawData(1, 4);
        rawData = rawData(length(index)+1:end, :);
    end
end
end