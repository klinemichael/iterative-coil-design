function [separatedCorners, connections] = SeparateCornersCylinder(data, innerRadius, outerRadius, height, centerPoint)

separatedCurves = {};
for i = 1:length(data)
    separatedCurves{i} = data{i}.points;
end

% figure(1),clf
% hold on
separatedCorners = {};
connections = {};
for j = 1:length(separatedCurves)
    corners = zeros(1, size(separatedCurves{j}, 2));
    for i = 1:size(separatedCurves{j}, 2)
        x = separatedCurves{j}(1, i) - centerPoint(1);
        y = separatedCurves{j}(2, i) - centerPoint(2);
        z = separatedCurves{j}(3, i) - centerPoint(3);
        r = sqrt(x^2 + y^2);
        angle = atan(y/x);
        if x<0
            angle = angle + pi;
        end
        
        if abs(r - innerRadius) < abs(r - outerRadius)
            nearestCorner = innerRadius * [cos(angle), sin(angle)];
        else
            nearestCorner = outerRadius * [cos(angle), sin(angle)];
        end
        
        if z < 0
            nearestCorner = [nearestCorner, -height/2];
        else
            nearestCorner = [nearestCorner, height/2];
        end
        
        if pdist([x, y, z; nearestCorner], 'euclidean') < 1e-3
            separatedCurves{j}(:, i) = nearestCorner';
            corners(i) = 1;
        end
    end
    
    idx = find(corners == 1);
    for i = length(idx)-1:-1:1
        if idx(i+1)-idx(i) == 1
            separatedCurves{j}(:, idx(i+1)) = [];
            corners(idx(i+1)) = [];
        end
    end
    
    idx = find(corners == 1);
    in1 = 1;
    connections{end+1} = length(separatedCorners)+1:length(separatedCorners)+length(idx)+1;%zeros(1, length(idx)+1);
    for i = 1:length(idx)
%         plot3(separatedCurves{j}(1, idx(i)), separatedCurves{j}(2, idx(i)), separatedCurves{j}(3, idx(i)), 'x');
        separatedCorners{end + 1}.points = separatedCurves{j}(:, in1:idx(i));
        in1 = idx(i);
        separatedCorners{end}.potential = data{j}.potential;
    end
    
    separatedCorners{end+1}.points = separatedCurves{j}(:, in1:end);
    separatedCorners{end}.potential = data{j}.potential;
end
% hold off

%% Check if each point is at the edge of the geometry and isn't an endpoint, and if so, make it a corner
symmetry = 8;
for j = 1:length(separatedCorners)
    if symmetry == 8
         for i = size(separatedCorners{j}.points, 2)-1:-1:2
             x = separatedCorners{j}.points(1, i) - centerPoint(1);
             y = separatedCorners{j}.points(2, i) - centerPoint(2);
             z = separatedCorners{j}.points(3, i) - centerPoint(3);
             
             if x == 0 || y == 0 || z == 0
                 separatedCorners{end+1}.points = separatedCorners{j}.points(:, i:end);
                 separatedCorners{end}.potential = separatedCorners{j}.potential;
                 separatedCorners{j}.points = separatedCorners{j}.points(:, 1:i);
                 for k = 1:length(connections)
                     if ismember(j, connections{k})
                         idx = find(connections{k} == j);
                         connections{k} = [connections{k}(1:idx), length(separatedCorners), connections{k}(idx+1:end)];
                     end
                 end
             end
         end
    else
    end
end

end