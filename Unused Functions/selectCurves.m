function repairedPoints = selectCurves(points)
% points - Cell array with each cell containing an array of points along curve

h = gcf;clf
hold on
for i = 1:length(points)
    plot3(points{i}(1,:), points{i}(2,:), points{i}(3,:), 'o-', 'LineWidth', 1); 
end
hold off
global curveIdx;
curveIdx = zeros(1, length(points));
global pointIdx;
pointIdx = zeros(1, size(cell2mat(points), 2));
global pointsArr;
pointsArr = points;
set(h, 'WindowButtonDownFcn', @mybttnfcn)
set(gcf, 'KeyPressFcn', @keyEvent);
waitfor(gca);
%disp('All Done! Selected Curves Are...')
%disp(find(curveIdx==1));
repairedPoints = pointsArr;
end