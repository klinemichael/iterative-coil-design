function u = evalDT(DT, points, potentials)

[ti, bc] = pointLocation(DT, points);

idx = find(isnan(ti));
ti(idx) = [];
bc(idx, :) = [];

triVals = potentials(DT(ti,:));
u = dot(bc',triVals')';

for i = 1:length(idx)
    if idx(i) > length(u)
        u = [u(1:idx(i)-1); NaN];
    else
        u = [u(1:idx(i)-1); NaN; u(idx(i):end)];
    end
end

end