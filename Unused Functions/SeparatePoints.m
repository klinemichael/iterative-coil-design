function separatedPoints = SeparatePoints(data)

% Remove Cells with Only One Point
for i = length(data):-1:1
    if size(unique(data{i}.points', 'rows')', 2) < 2
        data(i) = [];
    end
end

curves = {};
potentials = zeros(length(data));
for i = 1:length(data)
    curves{i} = data{i}.points;
    potentials(i) = data{i}.potential;
end

separatedPoints = {};
for i = 1:length(curves)
    [idx, isnoise] = DBSCAN(curves{i}', 1e-3, 5);
    minIdx = min(idx);
    maxIdx = max(idx);
    for j = minIdx:maxIdx
        separatedPoints{end+1}.points = curves{i}(:, find(idx==j));
        separatedPoints{end}.potential = potentials(i);
    end
end

end