import com.comsol.model.*;
import com.comsol.model.util.*;
ModelUtil.showProgress(true);

model_path = 'C:/Users/Michael/Desktop/CreateCoil/'; % Will need to change for each computer

%============================ PSI Coil ===================================%
% model_file = [model_path, 'PSI Coil.mph'];
% contour_file = 'C:/Users/Michael/Desktop/CreateCoil/PSICoil.csv';
% plot_group = 'pg1';
% evaluation_selName = 'cyl4';
% centerPoint = [0, 0, 0];
% innerRadius = 0.11;
% outerRadius = 0; % 0 for single-wound
% height = 1;
% endcaps = 0; % 0 for no endcaps, 1 for endcaps
% innerFieldDirection = [0, 1, 0]; % Direction of field inside inner cylinder
% 
% separationParam = 0; % st. devs, usually 2-4, set to 0 to not separate
% straightLineTol = 1e-5;
% smoothParam = 0.8; % 0-1, 0 is linear fit, 1 is natural cubic spline
% closeLoopTol = 5e-3;
% minDist = 5e-3; % minimum distance between adjacent points
% 
% symmetry = 8;
%=========================================================================%

%====================== Double Cos-Theta Coil ============================%
model_file = [model_path, 'DoubleCosThetaCoil2.mph'];
contour_file = 'C:/Users/Michael/Desktop/CreateCoil/CosThetaCoil.csv';
plot_group = 'pg2';
evaluation_selName = 'cyl4';
centerPoint = [0, 0, 0];
innerRadius = 5 * 0.0254;
outerRadius = 7 * 0.0254; % 0 for single-wound
height = 8 * 0.0254;
endcaps = 1; % 0 for no endcaps, 1 for endcaps
innerFieldDirection = [0, 1, 0]; % Direction of field inside inner cylinder

separationParam = 0; % st. devs, usually 2-4, set to 0 to not separate
straightLineTol = 1e-5;
smoothParam = 0.001; % 0-1, 0 is linear fit, 1 is natural cubic spline
closeLoopTol = 1e-3;
minDist = 5e-3; % minimum distance between adjacent points

symmetry = 8;
%=========================================================================%

loop_file = 'C:/Users/Michael/Desktop/CreateCoil/FinalLoops.csv';
% loop_file = 'C:/Users/Michael/Desktop/CreateCoil/LoopsIter2.csv';

% Number of Iterations
maxIters = 5;

%% To Do List
% - Play around with the meshes to see if the messy equipotentials with
% small loops around the main ones can be avoided

%% Load COMSOL Model and Contour Plot
disp('Loading COMSOL Model');
model_file = [model_path, 'Iter1.mph'];
[contour, model] = ExtractContoursFromModel(model_file, plot_group); sortPoints = true; formLoops = true;

%% Load Pre-Sorted Points
% contour_file = 'C:/Users/Michael/Desktop/CreateCoil/tempPoints.csv';
% contour = LoadPreSortedContours(contour_file); sortPoints = false; formLoops = true;

%% Load Pre-Formed Loops
% contour = LoadPreFormedLoops(loop_file); sortPoints = false; formLoops = false;

allLoops = {};
error = zeros(1, maxIters);
for i = 1:maxIters
    disp(['%%%%%%%%%%%%%% Iteration ' num2str(i) ' %%%%%%%%%%%%%%']);
    
    if formLoops
        %% Snap Points to Geometry
        snappedPoints = FollowGeometry(contour, centerPoint, innerRadius, outerRadius, height, endcaps);
        
        densitySeparatedPoints = SeparatePoints(snappedPoints);
        
        %% Sort Points Using Travelling Salesman Algorithm
        if sortPoints
            tic
            disp('Sorting Points');
            sortedPoints = SortPoints_TravelingSalesman(densitySeparatedPoints);
            time = toc;
            disp(['Elapsed time is ' num2str(floor(time / 60)) 'min and ' num2str(floor(rem(time, 60))) 'sec']); 
        else
            sortedPoints = snappedPoints;
        end
        
        %% Separate Out When There Are 2 Distinct Curves
        disp('Separating Curves');
        separatedCurves = SeparateCurves(sortedPoints, separationParam, innerRadius, outerRadius, centerPoint);
%         finalLoops = {};
%         for j = 1:length(separatedCurves)
%             finalLoops{j} = separatedCurves{j}.points;
%         end
        
        %% Find Corners
        disp('Finding Corners');
%         separatedCorners = SeparateCorners(separatedCurves);
        [separatedCorners, connections] = SeparateCornersCylinder(separatedCurves, innerRadius, outerRadius, height, centerPoint);
%         plotCoil(separatedCorners);
%         return
%         figure(1),clf
%         plotCoil(separatedCorners, [], false, false);

        %% Find Small Loops
        [removedSmallLoops, connections] = RemoveSmallLoops(separatedCorners, symmetry, innerRadius, outerRadius, height, centerPoint, connections);
%         figure(1),clf
%         plotCoil(removedSmallLoops, [], false, false);
%         return
        
        %% Manually Repair Points
        %repairedPoints = selectCurves(separatedCorners); %%% Not Needed

        %% Find Straight Lines
        disp('Finding Straight Lines');
        adjustedPoints = FindStraightLines(removedSmallLoops, straightLineTol);

        %% Find Cubic Spline And Make New Points
        disp('Finding Cubic Spline');
        [splines, smoothedPoints] = SmoothingSpline(adjustedPoints, smoothParam, minDist);
        
        %% Connect Curves
        disp('Connecting Curves');
        connectedCurves = ConnectCurves(smoothedPoints, connections);
        
        %% Mirror Points for Symmetry
        disp('Mirroring Curves for Symmetry');
        mirroredPoints = MirrorPoints(connectedCurves, symmetry, centerPoint);
        
        %% Re-joining Curves to Form Closed Loops
        disp('Closing Loops');
        [loops, pointsLeftOver] = CloseLoops(mirroredPoints, closeLoopTol);
%         figure(1),clf
%         plotCoil(loops);
%         return
        
        %% Manually Close Rest of Loops
        if ~isempty(pointsLeftOver)
            disp('Manually Close Remaining Loops');
            finalLoops = ManuallyCloseLoops(loops, pointsLeftOver);
        else
            finalLoops = loops;
        end

    else
        finalLoops = contour;
    end
    
    %% If Multiple Points are Close Together, Replace Them with One Point
%     disp('Fixing Points Too Close to Each Other');
    finalLoops = ReplaceDuplicatePoints(finalLoops, minDist/2);
    
    figure(2),clf
    plotCoil(finalLoops);
    title('Pre-Snapped Coil');
    
    %% Force Loops to Follow Geometry
    disp('Snapping Loops to Geometry');
    finalLoops = FollowGeometry(finalLoops, centerPoint, innerRadius, outerRadius, height, endcaps);
    
    %% Solve For Direction of Current
    disp('Finding Direction of Current');
    model = mphopen(model_file);
    finalLoops = FindDirection(finalLoops, model, innerFieldDirection, innerRadius, outerRadius, centerPoint); % Not General

    %% Plot Coil
    figure(1),clf
    plotCoil(finalLoops);
    title('Full Coil');
%     return
    
    %% Save Loops
    file_name = ['C:/Users/Michael/Desktop/CreateCoil/LoopsIter' num2str(i) '.csv'];
    SavePoints(finalLoops, file_name);
    
    %% Import Windings Into Model
    [contour, error(i)] = ImportPointsPSI(finalLoops, model, plot_group, evaluation_selName, i);
    
    %% Check for Stopping Condition
    disp(error(1:i));
    if i > 10
        normalizedError = error / error(1);
        
        if abs(normalizedError(i) - normalizedError(i-1)) < 1e-4
            disp('Stopping Condition Met');
            return
        end
    end
    
    formLoops = true;
    sortPoints = true;
end