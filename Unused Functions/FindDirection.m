function loops = FindDirection(loops, model, innerFieldDirection, innerRadius, outerRadius, centerPoint)

%======================================================%
%
%   FindDirection makes sure each loop is going from point-to-point the
%       direction of the current
%
%%% ARGUMENTS %%%
%
%                 loops: Cell array where each cell is a matrix containing the
%                        points along each loop
%
%                 model: Object containing the COMSOL model
%
%   innerFieldDirection: 1x3 array containing a vector corresponding to the
%                        field direction inside the coil
%
%           innerRadius: Inner radius of the coil
%
%           outerRadius: Outer radius of the coil, set to 0 for a single-wound coil
%
%           centerPoint: 1x3 array containing the center point of the coil
%
%%% OUTPUTS %%%
%
%                 loops: Cell array where each cell is a matrix containing the
%                        points along each loop
%
%%% Created by: Michael Kline %%%
%======================================================%

for j = 1:length(loops)

     % Find Center Point
    loopCenter = mean(loops{j}, 2);
    
    if outerRadius > innerRadius
        try
            [Bx, By, Bz] = mphinterp(model, {'mfnc.Bx', 'mfnc.By', 'mfnc.Bz'}, 'coord', loopCenter, 'dataset', 'dset2');
        catch
            [Bx, By, Bz] = mphinterp(model, {'mfnc2.Bx', 'mfnc2.By', 'mfnc2.Bz'}, 'coord', loopCenter, 'dataset', 'dset2');
        end
        
        D = [0; 0; 0];
        for i = 1:size(loops{j}, 2)-1
            iSeg = loops{j}(:, i);
            fSeg = loops{j}(:, i+1);

            vRi = loopCenter - iSeg;
            vRf = loopCenter - fSeg;

            D = D + cross(vRi,vRf);
        end
        
        signB = [Bx, By, Bz]*D;
        % Flip, if Necessary
        if signB < 0
            loops{j} = fliplr(loops{j});
        end
    else

        % Find Direction of Field from Loop
        D = [0; 0; 0];
        for i = 1:size(loops{j}, 2)-1
            iSeg = loops{j}(:, i);
            fSeg = loops{j}(:, i+1);

            vRi = loopCenter - iSeg;
            vRf = loopCenter - fSeg;

            D = D + cross(vRi,vRf);
        end
        signB = innerFieldDirection*D;

        % Check if loop is an inner loop or an outer loop
        tmpLoops = loops{j};
        tmpLoops(1, :) = tmpLoops(1,:) - centerPoint(1);
        tmpLoops(2, :) = tmpLoops(2,:) - centerPoint(2);
        tmpLoops(3, :) = tmpLoops(3,:) - centerPoint(3);
        radii = vecnorm(tmpLoops(1:2, :));
        maxRadius = max(radii);
        if abs(outerRadius - maxRadius) < abs(maxRadius - innerRadius) && outerRadius > innerRadius % Outer Loop
            % Flip, if Necessary
            if signB > 0
                loops{j} = fliplr(loops{j});
            end
        else % Inner Loop
            % Flip, if Necessary
            if signB < 0
                loops{j} = fliplr(loops{j});
            end
        end
    end
end

end