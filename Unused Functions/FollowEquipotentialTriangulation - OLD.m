function curve = FollowEquipotentialTriangulation(data, us, distStep)

%======================================================%
%
%   FollowEquipotentialTriangulation takes the value of the scalar potential on one 2D surface
%       and finds the equipotential curves
%
%%% ARGUMENTS %%%
%
%       data: Matrix containing the points and the potential at the
%             point. The first column is the x-coordinate, the second column is
%             the y-coordinate, the third column is the z-coordinate, and the
%             fourth column is the potential
%
%         us: Value of each potential to find the equipotential curve
%
%   distStep: Distance between points along the equipotential
%
%%% OUTPUTS %%%
%
%      curve: Cell array where each cell is a struct containing the
%             points along the equipotential and the potential of the curve
%
%%% Created by: Michael Kline %%%
%======================================================%

data(:, 1) = abs(data(:, 1));
data(:, 2) = abs(data(:, 2));

minX = min(data(:, 1));
maxX = max(data(:, 1));
minY = min(data(:, 2));
maxY = max(data(:, 2));

data(find(ismembertol(data(:, 1), minX, 1E-6)), 1) = minX;
data(find(ismembertol(data(:, 1), maxX, 1E-6)), 1) = maxX;
data(find(ismembertol(data(:, 2), minY, 1E-6)), 2) = minY;
data(find(ismembertol(data(:, 2), maxY, 1E-6)), 2) = maxY;

k = convhull(data(:, 1), data(:, 2));
constraints = zeros(length(k)-1, 2);
for i = 1:length(k)-1
    constraints(i, :) = [k(i), k(i+1)];
end

DT = delaunayTriangulation(data(:, 1), data(:, 2), constraints);
IO = isInterior(DT);
ConnectivityList = DT(IO, :);
DT = triangulation(ConnectivityList, DT.Points);

curve = cell(1, length(us));
Xs = minX:(maxX-minX)/200:maxX;
Ys = minY:(maxY-minY)/200:maxY;
pointsX = evalDT(DT, [Xs; zeros(size(Xs))]', data(:, 3));
pointsY = evalDT(DT, [zeros(size(Ys)); Ys]', data(:, 3));
maxUx = max(pointsX); minUx = min(pointsX);
maxUy = max(pointsY); minUy = min(pointsY);

idx = find(isnan(pointsX)); pointsX(idx) = []; Xs(idx) = [];
idx = find(isnan(pointsY)); pointsY(idx) = []; Ys(idx) = [];

for i = 1:length(us)

    if (us(i) <= maxUx) && (us(i) >= minUx)
        [indices, dists] = knnsearch(pointsX, us(i), 'K', 2);
        startpoint = [(dists(2)*Xs(indices(1)) + dists(1)*Xs(indices(2))) / (dists(1) + dists(2)), 0];
        ux = gradInterp(DT, startpoint, data(:, 3));
        if ux < 0
            signDir = -1;
        else
            signDir = 1;
        end
    elseif (us(i) <= maxUy) && (us(i) >= minUy)
        [indices, dists] = knnsearch(pointsY, us(i), 'K', 2);
        startpoint = [0, (dists(2)*Ys(indices(1)) + dists(1)*Ys(indices(2))) / (dists(1) + dists(2))];
        [~, uy] = gradInterp(DT, startpoint, data(:, 3));
        if uy > 0
            signDir = -1;
        else
            signDir = 1;
        end
    else
        continue
    end

    curve{i} = [];
    curve{i}(:, 1) = startpoint';
    while ~isnan(evalDT(DT, startpoint, data(:, 3))) && ((startpoint(1) >= minX) && (startpoint(1) <= maxX) && (startpoint(2) >= minY) && (startpoint(2) <= maxY))

        % Calculate Gradient of Scalar Potential
        u = evalDT(DT, startpoint, data(:, 3));
        [ux, uy] = gradInterp(DT, startpoint, data(:, 3));
        if isnan(ux), ux = 0; end
        if isnan(uy), uy = 0; end

        % Fix Point to Equipotential
        gradu = [ux, uy];
        dirU = sign(us(i) - u);
        xs = startpoint(1):dirU*distStep/2*gradu(1):startpoint(1)+dirU*distStep*2*gradu(1);
        ys = startpoint(2):dirU*distStep/2*gradu(2):startpoint(2)+dirU*distStep*2*gradu(2);
        if length(xs) < 1, xs = startpoint(1)*ones(size(ys)); end
        if length(ys) < 1, ys = startpoint(2)*ones(size(xs)); end
        try
            u2 = evalDT(DT, [xs; ys]', data(:, 3));
        catch
            u2 = [];
        end
        if ~isempty(u2) && (us(i) <= max(u2)) && (us(i) >= min(u2))
            [indices, dists] = knnsearch(u2, us(i), 'K', 2);
            startpoint = (dists(2)*[xs(indices(1)), ys(indices(1))] + dists(1)*[xs(indices(2)), ys(indices(2))]) / (dists(1) + dists(2));
            curve{i}(:, end) = startpoint';
            [ux, uy] = gradInterp(DT, startpoint, data(:, 3));
            if isnan(ux), ux = 0; end
            if isnan(uy), uy = 0; end
        end

        dirEquipotential = cross([0, 0, 1], [ux, uy, 0]);
        dirEquipotential = dirEquipotential(1:2) / norm(dirEquipotential(1:2));

        startpoint = startpoint + signDir*distStep*dirEquipotential;
        curve{i}(:, end+1) = startpoint';
    end
    if signDir > 0
        curve{i} = fliplr(curve{i});
    end
end

end