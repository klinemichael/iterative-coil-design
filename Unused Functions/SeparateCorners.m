function separatedCorners = SeparateCorners(data)

separatedCurves = {};
for i = 1:length(data)
    separatedCurves{i} = data{i}.points;
end

separatedCorners = {};
for j = 1:length(separatedCurves)
    angles = [];
    % Find Angle Between Nearest Points
    for i = 2:size(separatedCurves{j}, 2) - 1
        %A = finalContour{j}(:, i);
        %B = finalContour{j}(:, i - 1);
        %C = finalContour{j}(:, i + 1);
        %v1 = (B - A) / norm(B - A);
        %v2 = (C - A) / norm(C - A);
        %angles(i) = acos(v1' * v2);
        angles(i) = acos(((separatedCurves{j}(:, i - 1) - separatedCurves{j}(:, i)) / norm(separatedCurves{j}(:, i - 1) - separatedCurves{j}(:, i)))' * ((separatedCurves{j}(:, i + 1) - separatedCurves{j}(:, i)) / norm(separatedCurves{j}(:, i + 1) - separatedCurves{j}(:, i))));
    end
    %angles(1) = acos(((finalContour{j}(:, end - 1) - finalContour{j}(:, 1)) / norm(finalContour{j}(:, end - 1) - finalContour{j}(:, 1)))' * ((finalContour{j}(:, 2) - finalContour{j}(:, 1)) / norm(finalContour{j}(:, 2) - finalContour{j}(:, 1))));
    
    % Separate Curves By Corners - if it is a loop and has no corners, this could give cubic spline a corner at first/last point even if it isn't supposed to have one, but probably won't affect things too much
    angles(1) = pi;
    index = find(angles < 2 * pi / 3);
    if ~isempty(index)
        in1 = 1;
        for i = 1:length(index)
            in2 = index(i);
            separatedCorners{end + 1}.points = separatedCurves{j}(:, in1:in2);
            separatedCorners{end}.potential = data{j}.potential;
            if i < length(index) && index(i+1) - index(i) == 1 % Two corners in a row -- separate into two curves
                in1 = in2 + 1;
            else
                in1 = in2;
            end
        end
        if in2 < length(angles)
            separatedCorners{end + 1}.points = separatedCurves{j}(:, in1:end);
            separatedCorners{end}.potential = data{j}.potential;
        end
    else
        separatedCorners{end + 1}.points = separatedCurves{j};
        separatedCorners{end}.potential = data{j}.potential;
    end
end
    
end