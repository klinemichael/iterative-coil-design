function [loops, pointsLeftOver] = CloseLoops(data, tol)

finalPoints = {};
finalPotentials = zeros(1, length(data));
for i = 1:length(data)
    finalPoints{i} = data{i}.points;
    finalPotentials(i) = data{i}.potential;
end

loops = {};
pointsLeftOver = {};

for i = 1:length(finalPoints)
    for j = 1:length(finalPoints)
        if (i ~= j) && (size(finalPoints{i}, 1) > 0) && (size(finalPoints{j}, 1) > 0) && (finalPotentials(i) == finalPotentials(j))
            if ismembertol(finalPoints{i}(:, 1)', finalPoints{j}(:, end)', tol, 'ByRows', true)
                finalPoints{i} = [finalPoints{j}, finalPoints{i}(:, 2:end)];
                finalPoints{j} = [];
                % disp(['Curve ', num2str(j), ' attaches to curve ', num2str(i)]);
            elseif ismembertol(finalPoints{i}(:, end)', finalPoints{j}(:, 1)', tol, 'ByRows', true)
                finalPoints{i} = [finalPoints{i}, finalPoints{j}(:, 2:end)];
                finalPoints{j} = [];
                % disp(['Curve ', num2str(i), ' attaches to curve ', num2str(j)]);
            end
        end
    end
end

% Remove Cells With Only One Point
for j = length(finalPoints):-1:1
    if size(unique(finalPoints{j}', 'rows')', 2) <= 1
        finalPoints(j) = [];
        finalPotentials(j) = [];
    end
end

loops = {};
pointsLeftOver = {};
% Check if Each Cell is a Loop
for j = 1:length(finalPoints)
    x = finalPoints{j}(1,:);
    y = finalPoints{j}(2,:);
    z = finalPoints{j}(3,:);
    
    dist = [];
    for i = 1:length(x)-1
        dist(i) = pdist([x(i) y(i) z(i); x(i+1) y(i+1) z(i+1)], 'euclidean');
    end
%    dist = zscore(dist);
    
    meanDist = mean(dist);
    stdDist = std(dist);
    endZScore = (pdist([x(end) y(end) z(end); x(1) y(1) z(1)], 'euclidean') - meanDist) / stdDist;
    
    if endZScore < 10
        loops{end + 1} = finalPoints{j};
    else
        pointsLeftOver{end + 1}.points = finalPoints{j};
        pointsLeftOver{end}.potential = finalPotentials(j);
    end
end

% % % Check if Each Cell is a Loop
% % for i = 1:length(finalPoints)
% %     if ~sum(~ismembertol(finalPoints{i}(:, 1), finalPoints{i}(:, end), tol))
% %         loops{end + 1} = finalPoints{i};
% %     else
% %         pointsLeftOver{end + 1}.points = finalPoints{i};
% %         pointsLeftOver{end}.potential = finalPotentials(i);
% %     end
% % end
% % 

% Remove Duplicate Points from Closed Loops
for i = 1:length(loops)
    loops{i} = loops{i}(:, 1:end-1);
end

end