function sortedPoints = SortPoints_TravelingSalesman(data)

points = {};
for i = 1:length(data)
    points{i} = data{i}.points;
end
    
sortedPoints = {};
resultStruct = {};
for j = 1:length(points)
    fprintf(1, [num2str(j) '/' num2str(length(points)) '...']);
    
    userConfig = struct('xy',[transpose(points{j}(1,:)) transpose(points{j}(2,:)) transpose(points{j}(3,:))], 'showProg', false, 'showResult', false, 'showWaitbar', true);
    resultStruct{j} = tsp_nn(userConfig);
    optRoute = resultStruct{j}.optRoute;
    sortedPoints{j}.points = points{j};
    for i = 1:length(optRoute)
        n = optRoute(i);
        sortedPoints{j}.points(:,i) = points{j}(:,n);
    end
    sortedPoints{j}.potential = data{j}.potential;
end
disp('DONE!');

end