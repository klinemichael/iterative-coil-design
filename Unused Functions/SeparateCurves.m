function separatedCurves = SeparateCurves(data, stDev_separation, innerRadius, outerRadius, centerPoint)

if stDev_separation > 0
    
    sortedPoints = {};
    for i = 1:length(data)
        sortedPoints{i} = data{i}.points;
    end

    separatedCurves = {};
    for j = 1:length(sortedPoints)
        x = sortedPoints{j}(1,:);
        y = sortedPoints{j}(2,:);
        z = sortedPoints{j}(3,:);

        dist = [];
        for i = 1:length(x)-1
            dist(i) = pdist([x(i) y(i) z(i); x(i+1) y(i+1) z(i+1)], 'euclidean');
        end
        dist(end + 1) = pdist([x(end) y(end) z(end); x(1) y(1) z(1)], 'euclidean');
        dist = zscore(dist);
        if dist(end) < stDev_separation % If first and last points connect, make sure they still connect after separating curves
            index = find(dist > stDev_separation);
            if ~isempty(index)
                in1 = index(1) + 1;
                for i = 2:length(index)
                    in2 = index(i);
                    separatedCurves{end + 1}.points = sortedPoints{j}(:, in1:in2);
                    separatedCurves{end}.potential = data{j}.potential;
                    in1 = in2 + 1;
                end
                separatedCurves{end + 1}.points = [sortedPoints{j}(:, in1:end), sortedPoints{j}(:, 1:index(1))]; % Combine end of curve and beginning of beginning of curve
                separatedCurves{end}.potential = data{j}.potential;
            else
                separatedCurves{end + 1}.points = sortedPoints{j};
                separatedCurves{end}.points(:, end+1) = sortedPoints{j}(:, 1); % Add first point to end of closed loop
                separatedCurves{end}.potential = data{j}.potential;
            end
        else
            dist = dist(1:end-1);
            index = find(dist > stDev_separation); % Find indices where the distance is > 3 st. dev.
            if ~isempty(index)
                in1 = 1;
                for i = 1:length(index)
                    in2 = index(i);
                    separatedCurves{end + 1}.points = sortedPoints{j}(:, in1:in2);
                    separatedCurves{end}.potential = data{j}.potential;
                    in1 = in2 + 1;
                end
                if in2 < length(dist)
                    separatedCurves{end + 1}.points = sortedPoints{j}(:, in1:end);
                    separatedCurves{end}.potential = data{j}.potential;
                end
            else
                separatedCurves{end + 1}.points = sortedPoints{j};
                separatedCurves{end}.potential = data{j}.potential;
            end
        end

        %figure(j+100),clf
        %hold on
        %plot(1:1:length(dist), dist)
        %plot(1:1:length(dist), ones(length(dist)) * stDev_separation)
        %hold off
    end

    % Remove Cells With Only One Point
    for j = length(separatedCurves):-1:1
        if size(unique(separatedCurves{j}.points', 'rows')', 2) <= 1
            disp('Found Cell With Only One Point!!!');
            separatedCurves(i) = [];
        end
    end

    % Plot Separated Curves
    %for j = 1:length(sortedPoints)
    %    figure(j),clf
    %    hold on
    %    plot3(sortedPoints{j}(1,:), sortedPoints{j}(2,:), sortedPoints{j}(3,:), 'k.');
    %    for i = 1:length(separatedCurves)
    %        if sum(sum(ismember(separatedCurves{i}, sortedPoints{j}))) == numel(separatedCurves{i})
    %            plot3(separatedCurves{i}(1,:), separatedCurves{i}(2,:), separatedCurves{i}(3,:), 'o');
    %        end
    %    end
    %    hold off
    %end
    %
    %sumMatches = zeros(1, length(separatedCurves));
    %for i = 1:length(separatedCurves)
    %    for j = 1:length(sortedPoints)
    %        if sum(sum(ismember(separatedCurves{i}, sortedPoints{j}))) == numel(separatedCurves{i})
    %            sumMatches(i) = sumMatches(i) + 1;
    %        end
    %    end
    %end
    %
    %end
else
    % Start the curve where the max distance is between the endpoints
    
    for i = 1:length(data)
        x = data{i}.points(1,:);
        y = data{i}.points(2,:);
        z = data{i}.points(3,:);

        dist = zeros(size(x));
        for j = 1:length(x)-1
            dist(j) = pdist([x(j) y(j) z(j); x(j+1) y(j+1) z(j+1)], 'euclidean');
        end
        dist(end) = pdist([x(end) y(end) z(end); x(1) y(1) z(1)], 'euclidean');
        
        [~, idx] = max(dist);
        if idx < length(dist)
            data{i}.points = [data{i}.points(:, idx+1:end), data{i}.points(:, 1:idx)];
        end
    end
    
    separatedCurves = data;
end

end