function [newPoints, error] = ImportPointsPSI(points, model, plot_group, evaluation_selName, iter)

%% Imports
import com.comsol.model.*;
import com.comsol.model.util.*;

%% Check Each Loop Has Enough Points
for i = length(points):-1:1
    if size(unique(points{i}', 'rows')', 2) <= 2
        points(i) = [];
    end
end

%% Import Windings
disp('Importing Windings');
selName = 'Windings';
try
    model.geom('geom1').selection.create(selName, 'CumulativeSelection');
catch
end
model.geom('geom1').create('if2', 'If');
model.geom("geom1").feature("if2").set("condition", "wires_on==1");
for j = 1:length(points)
%     model.geom('geom1').feature.create(['pol' num2str(j)], 'Polygon').set('source', 'table').set('table', points{j}').set('type', 'closed').set('contributeto', selName);
    
    if HasCorners(points{j})
        model.geom('geom1').feature.create(['pol' num2str(j)], 'Polygon').set('source', 'table').set('table', points{j}').set('type', 'closed').set('contributeto', selName);
    else
        model.geom('geom1').feature.create(['ic' num2str(j)], 'InterpolationCurve').set('source', 'table').set('table', points{j}').set('rtol', 0.001).set('contributeto', selName).set('type', 'closed');
    end
end

model.geom('geom1').create('WindingsBlock', 'Block');
model.geom('geom1').feature('WindingsBlock').set('size', {'10', '10', '10'});

model.geom('geom1').create('WindingsUnion', 'Union');
model.geom('geom1').feature('WindingsUnion').selection('input').named('Windings');

model.geom('geom1').create('WindingsIntersection', 'Intersection');
model.geom('geom1').feature('WindingsIntersection').selection('input').set({'WindingsBlock', 'WindingsUnion'});

model.geom('geom1').create('endif2', 'EndIf');

%% Set wires_on to 1 and Build Geometry
model.param.set('wires_on', '1', '1 for wires, 2 for box');
model.geom('geom1').run;

%% Set Mesh Around Windings
% model.mesh("mesh1").feature("edg1").feature("size1")
% model.mesh("mesh1").feature("edg1").feature("size1").set("custom", "off");
% model.mesh('mesh1').feature('edg1').feature('size1').set('hmin', '0.0001');

model.mesh('mesh1').feature('edg1').selection.named(['geom1_' selName '_edg']);

%% Add Current to Wires
% disp('Adding Current to Wires');
model.physics('mf').feature('edc1').selection.named(['geom1_' selName '_edg']); % Specify All Wires In Selection

% %% Run MFNC Study
% tic
% disp('Running MFNC Study');
% model.study('std1').run();
% time = toc;
% disp(['Elapsed time is ' num2str(floor(time / 60)) 'min and ' num2str(floor(rem(time, 60))) 'sec']); 

%% Run MF Study
tic
disp('Running MF Study');
model.study('std1').run();
time = toc;
disp(['Elapsed time is ' num2str(floor(time / 60)) 'min and ' num2str(floor(rem(time, 60))) 'sec']); 

%% Run MFNC2 Study
tic
disp('Running MFNC Study');
model.study('std2').run();
time = toc;
disp(['Elapsed time is ' num2str(floor(time / 60)) 'min and ' num2str(floor(rem(time, 60))) 'sec']); 

%% Extract New Contours
disp('Extracting New Contours');
% avgu2 = mphmean(model, 'u2', 'surface', 'dataset', 'dset3');
% avgsqrtu2 = mphmean(model, 'sqrt(u2)', 'surface', 'dataset', 'dset3');
% scale = avgu2 / avgsqrtu2;
% model.result(plot_group).feature('con1').set('expr', ['sqrt(u2)*' num2str(scale) '+u1']);

% % avgu3 = mphmean(model, 'u3', 'surface', 'selection', 'cyl5', 'dataset', 'dset4');
% % avgsqrtu3 = mphmean(model, 'sqrt(u3)', 'surface', 'selection', 'cyl5', 'dataset', 'dset4');
% % scale = avgu3 / avgsqrtu3;
% % model.result(plot_group).feature('con1').set('expr', ['sqrt(u3)*(' num2str(real(scale)) ')+u1']);
% % 
% % avgu4 = mphmean(model, 'u4', 'surface', 'selection', 'dif3', 'dataset', 'dset4');
% % avgsqrtu4 = mphmean(model, 'sqrt(u4)', 'surface', 'selection', 'dif3', 'dataset', 'dset4');
% % scale = avgu4 / avgsqrtu4;
% % model.result(plot_group).feature('con2').set('expr', ['sqrt(u4)*(' num2str(real(scale)) ')+u2']);

figure(10),clf
rawData = mphplot(model, plot_group);
data = {};
for i = 1:length(rawData)
    for j = 1:length(rawData{i})
        if strcmp(rawData{i}{j}.plottype, 'Contour')
            data{end+1}.points = rawData{i}{j}.p;
            data{end}.potential = rawData{i}{j}.d;
            data{end}.potential = data{end}.potential(1);
        end
    end
end
newPoints = data;

%% Calculate Error
disp('Calculating Error');
% expr = 'log10((field - mf.normB) / field)';
% avg = mphmean(model, expr, 'volume', 'selection', evaluation_selName);
% avg2 = mphmean(model, ['(' expr '-(' num2str(avg) '))^2'], 'volume', 'selection', evaluation_selName);
% error = sqrt(real(avg2));
error = sqrt(real(mphmean(model, ['(log10((field - mf.normB) / field)-(' num2str(mphmean(model, 'log10((field - mf.normB) / field)', 'volume', 'selection', evaluation_selName)) '))^2'], 'volume', 'selection', evaluation_selName)));
% error = 1/iter;

%% Save Model
disp('Saving Model');
try
    mphsave(model, ['C:/Users/Michael/Desktop/CreateCoil/Iter' num2str(iter) '.mph']);
catch
    fprintf(2, 'ERROR: Could not save model');
end

end