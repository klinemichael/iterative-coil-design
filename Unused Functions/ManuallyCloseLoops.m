function finalLoops = ManuallyCloseLoops(loops, pointsLeftOver)

disp("- Click to select a curve");
disp("- Ctrl+Click to select a point");
disp("- Press 'm' to merge selected curves");
disp("- Press 'd' to delete selected curve(s)");
disp("- Press 'u' to de-select all");
disp("- Press 's' to separate curve at two adjacent selected points");
disp("- Press 'r' to restart the equipotential");
disp("- Press 'c' to connect two curves at selected endpoints");

equipotentials = zeros(1, length(pointsLeftOver));
for i = 1:length(pointsLeftOver)
    equipotentials(i) = pointsLeftOver{i}.potential;
end
equipotentials = unique(equipotentials);

finalLoops = loops;

for i = 1:length(equipotentials)
    % Plot the Current Equipotential Curve(s)
    figure(1),clf
    h = gcf;
    hold on
    points = {};
    for j = 1:length(pointsLeftOver)
        if pointsLeftOver{j}.potential == equipotentials(i)
            plot3(pointsLeftOver{j}.points(1,:), pointsLeftOver{j}.points(2,:), pointsLeftOver{j}.points(3,:), 'o-', 'LineWidth', 1);
            points{end + 1} = pointsLeftOver{j}.points;
        end
    end
    hold off
    title(['Equipotential: ' num2str(equipotentials(i))]);
    
    % Define Global Variables
    global curveIdx;
    curveIdx = zeros(1, length(points));
    global pointIdx;
    pointIdx = zeros(1, size(cell2mat(points), 2));
    global pointsArr;
    pointsArr = points;
    
    % Manually Select Loops
    set(h, 'WindowButtonDownFcn', @mybttnfcn)
    set(gcf, 'KeyPressFcn', {@keyEventLoop, points});
    waitfor(gca);
    finalLoops = [finalLoops pointsArr];
end

end