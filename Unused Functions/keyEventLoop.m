function keyEventLoop(h, ~, originalPoints)

key = get(h, 'CurrentKey');
global curveIdx;
global pointsArr;
global pointIdx;

%% Merge selected curves
if key == 'm' %% Merge Selected Curves
    curves = find(curveIdx==1);
    points = [];
    for i = 1:length(curves)
        points = [points, pointsArr{curves(i)}];
    end
    
    pointsArr(curves) = []; % Remove cells to be merged
    
    userConfig = struct('xy',[transpose(points(1,:)) transpose(points(2,:)) transpose(points(3,:))], 'showProg', false, 'showResult', false, 'showWaitbar', true);
    resultStruct = tsp_nn(userConfig);
    optRoute = resultStruct.optRoute;
    sortedPoints = 0 * points;
    for i = 1:length(optRoute)
        n = optRoute(i);
        sortedPoints(:,i) = points(:,n);
    end
    
    pointsArr{end + 1} = sortedPoints; % Add merged points back
    curveIdx = zeros(1, length(pointsArr));
    
    children = get(gca, 'children');
    delete(children);
    hold on
    for i = 1:length(pointsArr)
        plot3(pointsArr{i}(1,:), pointsArr{i}(2,:), pointsArr{i}(3,:), 'o-', 'LineWidth', 1); 
    end
    hold off
%% Delete selected curve(s)
elseif key == 'd' %% Delete Selected Loop(s)
    curves = find(curveIdx==1);
    
    pointsArr(curves) = [];
    curveIdx = zeros(1, length(pointsArr));
    
    % Reload Plot
    children = get(gca, 'children');
    delete(children);
    hold on
    for i = 1:length(pointsArr)
        plot3(pointsArr{i}(1,:), pointsArr{i}(2,:), pointsArr{i}(3,:), 'o-', 'LineWidth', 1); 
    end
    hold off
    
%     pointCloud = cell2mat(pointsArr);
%     pointsToDelete = pointCloud(:, find(pointIdx == 1));
%     for i = 1:length(pointsArr)
%         for j = 1:size(pointsToDelete, 2)
%             pointsArr{i}(:, find(all(bsxfun(@eq, pointsToDelete(:, j)', pointsArr{i}'), 2)' == 1)) = [];
%         end
%     end
%     
%     pointIdx = zeros(1, size(cell2mat(pointsArr), 2));
%     
%     children = get(gca, 'children');
%     delete(children);
%     hold on
%     for i = 1:length(pointsArr)
%         plot3(pointsArr{i}(1,:), pointsArr{i}(2,:), pointsArr{i}(3,:), 'o-', 'LineWidth', 1); 
%     end
%     hold off
%% De-select all
elseif key == 'u' %% De-select All
    curveIdx = zeros(1, length(pointsArr));
    pointIdx = zeros(1, size(cell2mat(pointsArr), 2));
    
    children = get(gca, 'children');
    delete(children);
    hold on
    for i = 1:length(pointsArr)
        plot3(pointsArr{i}(1,:), pointsArr{i}(2,:), pointsArr{i}(3,:), 'o-', 'LineWidth', 1); 
    end
    hold off
%% Separate Curve
elseif key == 's' %% Separate Curve at Two Adjacent Selected Points
    pointCloud = cell2mat(pointsArr);
    points = pointCloud(:, find(pointIdx == 1));
    
    % Check that exactly two points are selected
    if size(points, 2) == 2
        % Check they are on the same curve
        i = 1;
        while i <= length(pointsArr)
            if sum(ismember(points', pointsArr{i}', 'rows')) == 2
                % Check if they are adjacent. If adjacent, separate curve. If not adjacent, throw error
                [~, idx] = intersect(pointsArr{i}', points', 'rows');
                if abs(idx(1) - idx(2)) == 1
                    pointsArr{end + 1} = pointsArr{i}(:, 1:min(idx));
                    pointsArr{i} = pointsArr{i}(:, max(idx):end);
                else
                    fprintf(2, 'Points must be adjacent to each other\n');
                end
                i = length(pointsArr) + 2;
            else
                i = i + 1;
            end
        end
        if i ~= length(pointsArr) + 2
            fprintf(2, 'Points must be on the same curve\n');
        end
    else
        fprintf(2, 'Exactly two points must be selected\n');
    end
    
    curveIdx = zeros(1, length(pointsArr));
    pointIdx = zeros(1, size(cell2mat(pointsArr), 2));
    
    children = get(gca, 'children');
    delete(children);
    hold on
    for i = 1:length(pointsArr)
        plot3(pointsArr{i}(1,:), pointsArr{i}(2,:), pointsArr{i}(3,:), 'o-', 'LineWidth', 1); 
    end
    hold off
%% Right Arrow
elseif strcmp(key, 'rightarrow') % Select Point to Right
    pointCloud = cell2mat(pointsArr);
    idx = find(pointIdx == 1);
    
     % Check that exactly one point is selected
    if length(idx) == 1
        if idx < length(pointIdx)
            pointIdx(idx + 1) = 1;
        else
            pointIdx(1) = 1;
        end
    else
        fprintf(2, 'Must have exactly one point selected\n');
    end
    
    % Plot Selected Points
    idx = find(pointIdx == 1);
    children = get(gca, 'children');
    delete(children);
    hold on
    for i = 1:length(pointsArr)
          h = plot3(pointsArr{i}(1,:), pointsArr{i}(2,:), pointsArr{i}(3,:), 'o-', 'LineWidth', 1);
    end
    for i = 1:length(idx)
        h = plot3(pointCloud(1, idx(i)), pointCloud(2, idx(i)), pointCloud(3, idx(i)), 'r.-', 'MarkerSize', 30); 
    end
    hold off
    set(h,'Tag','pt');  % set its Tag property for later use
%% Left Arrow
elseif strcmp(key, 'leftarrow') % Select Point to Left
    pointCloud = cell2mat(pointsArr);
    idx = find(pointIdx == 1);
    
     % Check that exactly one point is selected
    if length(idx) == 1
        if idx > 1
            pointIdx(idx - 1) = 1;
        else
            pointIdx(end) = 1;
        end
    else
        fprintf(2, 'Must have exactly one point selected\n');
    end
    
    % Plot Selected Points
    idx = find(pointIdx == 1);
    children = get(gca, 'children');
    delete(children);
    hold on
    for i = 1:length(pointsArr)
          h = plot3(pointsArr{i}(1,:), pointsArr{i}(2,:), pointsArr{i}(3,:), 'o-', 'LineWidth', 1);
    end
    for i = 1:length(idx)
        h = plot3(pointCloud(1, idx(i)), pointCloud(2, idx(i)), pointCloud(3, idx(i)), 'r.-', 'MarkerSize', 30); 
    end
    hold off
    set(h,'Tag','pt');  % set its Tag property for later use
%% Restart
elseif key == 'r'
    curveIdx = zeros(1, length(originalPoints));
    pointIdx = zeros(1, size(cell2mat(originalPoints), 2));
    pointsArr = originalPoints;
    
    children = get(gca, 'children');
    delete(children);
    hold on
    for i = 1:length(pointsArr)
        plot3(pointsArr{i}(1,:), pointsArr{i}(2,:), pointsArr{i}(3,:), 'o-', 'LineWidth', 1); 
    end
    hold off
%% Connect Curves at Endpoints
elseif key == 'c'
    pointCloud = cell2mat(pointsArr);
    points = pointCloud(:, find(pointIdx == 1));
    
    % Make sure exactly two points are selected
    if size(points, 2) == 2
        % Find what curve each point is on
        selected = zeros(1, length(pointsArr));
        for i = 1:length(pointsArr)
            selected(i) = sum(ismember(points', pointsArr{i}', 'rows'));
        end
        idx = find(selected == 1);
        if length(idx) == 2
            % Make sure points are endpoints of curve
            [contained, index] = ismember(points(:, 1)', pointsArr{idx(1)}', 'rows');
            if contained  % point 1 -> curve 1, point 2 -> curve 2
                [~, index2] = ismember(points(:, 2)', pointsArr{idx(2)}', 'rows');
            else % point 1 -> curve 2, point 2 -> curve 1
                [~, index2] = ismember(points(:, 1)', pointsArr{idx(2)}', 'rows');
                [~, index] = ismember(points(:, 2)', pointsArr{idx(1)}', 'rows');
            end
            if index == 1 && index2 == size(pointsArr{idx(2)}, 2)
                % Merge Curves
                pointsArr{idx(1)} = [pointsArr{idx(2)}, pointsArr{idx(1)}];
                pointsArr(idx(2)) = [];
            elseif index == 1 && index2 == 1
                % Merge Curves
                pointsArr{idx(1)} = [fliplr(pointsArr{idx(2)}), pointsArr{idx(1)}];
                pointsArr(idx(2)) = [];
            elseif index == size(pointsArr{idx(1)}, 2) && index2 == 1
                % Merge Curves
                pointsArr{idx(1)} = [pointsArr{idx(1)}, pointsArr{idx(2)}];
                pointsArr(idx(2)) = [];
            elseif index == size(pointsArr{idx(1)}, 2) && index2 == size(pointsArr{idx(2)}, 2)
                % Merge Curves
                pointsArr{idx(1)} = [pointsArr{idx(1)}, fliplr(pointsArr{idx(2)})];
                pointsArr(idx(2)) = [];
            else
                fprintf(2, 'Points must be endpoints of curve\n');
            end
        else
            fprintf(2, 'Points must be on different curves\n');
        end
    else
        fprintf(2, 'Exactly two points must be selected\n');
    end
    
    curveIdx = zeros(1, length(pointsArr));
    pointIdx = zeros(1, size(cell2mat(pointsArr), 2));
    
    children = get(gca, 'children');
    delete(children);
    hold on
    for i = 1:length(pointsArr)
        plot3(pointsArr{i}(1,:), pointsArr{i}(2,:), pointsArr{i}(3,:), 'o-', 'LineWidth', 1); 
    end
    hold off
else
%    disp(key);
end

end